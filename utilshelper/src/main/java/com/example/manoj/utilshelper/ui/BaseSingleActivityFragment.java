package com.example.manoj.utilshelper.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;

/**
 * Created by manoj on 09/06/16.
 */
public abstract class BaseSingleActivityFragment extends Fragment implements View.OnClickListener {


    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, baseView);
        initViews(baseView);
        return baseView;
    }


    @Override
    public void onClick(View v) {

    }

    public boolean handleBackPress() {
        return false;
    }

    protected void hideSoftKeyBoard(EditText et) {
        if (null != et && context != null) {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

    protected void showSoftKeyBoard(EditText et) {
        if (null != et && context != null) {
            et.requestFocus();
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    protected abstract int getLayoutResourceId();

    protected abstract void initViews(View baseView);
}
