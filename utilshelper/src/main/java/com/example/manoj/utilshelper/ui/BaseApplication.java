package com.example.manoj.utilshelper.ui;

import android.app.Application;

import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.managers.SharedPrefsManager;

/**
 * Created by manoj on 14/02/16.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        Utils.init(this);
        SharedPrefsManager.initialize(this);
        super.onCreate();
    }

}
