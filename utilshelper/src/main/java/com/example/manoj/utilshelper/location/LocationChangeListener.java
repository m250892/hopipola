package com.example.manoj.utilshelper.location;

import android.location.Location;

public interface LocationChangeListener {
    void onLocationChange(Location location);
}
