package com.example.manoj.utilshelper.location;

import android.location.Location;
import android.util.LruCache;

import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.core.ArrayUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by manoj on 28/11/15.
 */
public class AddressSuggestionRequestController implements AddressSuggestionAsyncTask.AutocompleteInterface {

    public static AddressSuggestionRequestController instance;
    private ArrayList<String> requestQueue;
    private boolean isRequestInProgress;
    private HashMap<String, LRCAreaSuggestCallback> callbackMap;
    private LRCCurrentLocationCallback currentLocationCallback;
    private LruCache<String, List<String>> cacheData = new LruCache<String, List<String>>(10);

    private AddressSuggestionRequestController() {
        this.requestQueue = new ArrayList<>();
        this.callbackMap = new HashMap<>();
        this.isRequestInProgress = false;
    }

    public static synchronized AddressSuggestionRequestController getInstance() {
        if (null == instance) {
            instance = new AddressSuggestionRequestController();
        }
        return instance;
    }

    public void getSuggestionAreas(String inputText, LRCAreaSuggestCallback callback) {
        Utils.log.e("getSuggestionAreas for " + inputText);
        if (ArrayUtils.isCollectionFilled(cacheData.get(inputText))) {
            Utils.log.e("Fetch Suggestion from cache " + inputText);
            callback.onSuggestionLoaded(cacheData.get(inputText), inputText);
        } else {
            removePreviousRequest(inputText);
            requestQueue.add(0, inputText);
            this.callbackMap.put(inputText, callback);
            handleRequestQueueChange();
        }
    }

    public void getCurrentLocationAddress(Location location, LRCCurrentLocationCallback callback) {
        this.currentLocationCallback = callback;
        new AddressSuggestionAsyncTask(location, this).execute();

    }

    private void removePreviousRequest(String searchText) {
        for (int index = 0; index < requestQueue.size(); index++) {
            String text = requestQueue.get(index);
            if (text != null && text.equalsIgnoreCase(searchText)) {
                requestQueue.remove(index);
                callbackMap.remove(searchText);
                break;
            }
        }
    }

    private void handleRequestQueueChange() {
        if (this.isRequestInProgress == false) {
            //MainApplication.printLog("Request Queue Text : " + requestQueue.toString());
            if (requestQueue.size() > 0) {
                isRequestInProgress = true;
                String tmpText = requestQueue.get(0);
                LRCAreaSuggestCallback tmpCallback = callbackMap.get(tmpText);
                requestQueue.clear();
                callbackMap.clear();
                requestQueue.add(0, tmpText);
                callbackMap.put(tmpText, tmpCallback);
                new AddressSuggestionAsyncTask(requestQueue.get(0), this).execute();
            }
        }
    }

    @Override
    public void updateAutocompleteList(List newList, String searchText) {
        //MainApplication.printLog("updateAutocompleteList for text " + searchText);
        if (newList != null && searchText != null) {
            cacheData.put(searchText, newList);
        }
        for (int index = 0; index < requestQueue.size(); index++) {
            String inputText = requestQueue.get(index);
            if (inputText != null && inputText.equalsIgnoreCase(searchText)) {
                callbackMap.get(searchText).onSuggestionLoaded(newList, searchText);
                requestQueue.remove(index);
                break;
            }
        }
        isRequestInProgress = false;
        handleRequestQueueChange();
    }

    @Override
    public void onCurrentLocationAddessFetched(SearchItemModel result) {
        if (currentLocationCallback != null) {
            currentLocationCallback.onCurrentLocationAddressLoaded(result);
        }
    }

    public interface LRCCurrentLocationCallback {
        void onCurrentLocationAddressLoaded(SearchItemModel searchItemModel);
    }

    public interface LRCAreaSuggestCallback {
        void onSuggestionLoaded(List results, String inputText);
    }
}
