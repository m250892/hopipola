package com.example.manoj.utilshelper.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;


/**
 * Created by manoj on 14/02/16.
 */
public abstract class BaseActivity extends BaseSingleActivity implements IBaseFragmentController {
    private FragmentTransaction fragmentTransaction;

    @Override
    public boolean isForeground() {
        return false;
    }

    @Override
    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    }

    @Override
    public Fragment getTopFragmentInBackStack() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount <= 0) {
            return null;
        }
        FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount - 1);
        String fragmentTag = backStackEntry.getName();
        if (TextUtils.isEmpty(fragmentTag)) {
            throw new IllegalStateException("Fragment added without a tag");
        }
        return getSupportFragmentManager().findFragmentByTag(fragmentTag);
    }

    @Override
    public void popBackStackIfForeground() {
        if (getSupportFragmentManager() != null && isForeground()) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void popBackStack() {
        if (getSupportFragmentManager() != null) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = getCurrentFragment();
        if (currentFrag != null) {
            transaction.remove(currentFrag);
        }
        transaction.commit();
    }

    public void clearBackStack(boolean isInclusive) {
        if (getSupportFragmentManager() != null) {
            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            if (backStackEntryCount <= 0) {
                return;
            }
            FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager()
                    .getBackStackEntryAt(0);
            String fragmentTag = backStackEntry.getName();
            if (isInclusive) {
                getSupportFragmentManager()
                        .popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else {
                getSupportFragmentManager().popBackStack(fragmentTag, 0);
            }
        }
    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
        // Allow state loss by default
        addFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                           boolean addToBackStack) {
        // Allow state loss by default
        addFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
    }

    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack,
                                           boolean allowStateLoss) {
        addFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, allowStateLoss, getSupportFragmentManager());
    }


    public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack,
                                           boolean allowStateLoss, FragmentManager fragmentManager) {
        if (!getSupportFragmentManager().isDestroyed()) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .add(getFragmentContainerId(), fragmentToBeLoaded, fragmentToBeLoaded.getName());
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
            }
            Fragment currFragment = getCurrentFragment();
            if (null != currFragment) {
                currFragment.onPause();
            }
            if (allowStateLoss) {
                fragmentTransaction.commitAllowingStateLoss();
            } else {
                fragmentTransaction.commit();
            }
        }
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
        // Allow state loss by default
        replaceFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                               boolean addToBackStack) {
        // Allow state loss by default
        replaceFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
    }

    public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                               boolean addToBackStack, boolean allowStateLoss) {
        replaceFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, allowStateLoss, getSupportFragmentManager());
    }

    private void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack, boolean allowStateLoss, FragmentManager supportFragmentManager) {
        if (!getSupportFragmentManager().isDestroyed()) {
            fragmentTransaction = supportFragmentManager.beginTransaction();
            //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
            fragmentTransaction.replace(getFragmentContainerId(), fragmentToBeLoaded,
                    fragmentToBeLoaded.getName());
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
            }
            if (allowStateLoss) {
                fragmentTransaction.commitAllowingStateLoss();
            } else {
                fragmentTransaction.commit();
            }
        }
    }

    @Override
    public ActionBar getSupportActionBarM() {
        return getSupportActionBar();
    }

    @Override
    public void setSupportActionBarM(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }

    @Override
    public void intigrateToolbarWithDrawer(Toolbar toolbar) {
    }

    public abstract int getFragmentContainerId();
}
