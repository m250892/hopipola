package com.example.manoj.utilshelper.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.example.manoj.utilshelper.R;
import com.example.manoj.utilshelper.interfaces.IPermissionsCallback;
import com.example.manoj.utilshelper.managers.SharedPrefsManager;

import java.util.HashMap;

/**
 * Created by manoj on 09/06/16.
 */
public class BaseSingleActivity extends AppCompatActivity {

    protected HashMap<Integer, IPermissionsCallback> requestCodeToCallbackMap = new HashMap<>();

    protected int getStatusBarHeight() {
        int statusBarHeight = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
        }
        return statusBarHeight;
    }

    public void requestPermission(int requestCode, IPermissionsCallback permissionsCallback) {
        String permission = null;
        switch (requestCode) {
            case IPermissionsCallback.GET_ACCOUNTS:
                permission = Manifest.permission.GET_ACCOUNTS;
                break;
            case IPermissionsCallback.GET_FINE_LOCATION:
                permission = Manifest.permission.ACCESS_FINE_LOCATION;
                break;
            case IPermissionsCallback.GET_CORE_LOCATION:
                permission = Manifest.permission.ACCESS_COARSE_LOCATION;
                break;
            case IPermissionsCallback.READ_CONTACTS:
                permission = Manifest.permission.READ_CONTACTS;
                break;
            case IPermissionsCallback.READ_SMS:
                permission = Manifest.permission.READ_SMS;
                break;
            case IPermissionsCallback.RECEIVE_SMS:
                permission = Manifest.permission.RECEIVE_SMS;
                break;
            default:
                new RuntimeException("Permission not find in callbacks");
        }
        requestPermission(requestCode, permission, permissionsCallback);
    }

    protected void requestPermission(int requestCode, String permission, IPermissionsCallback permissionsCallback) {
        if (checkPermissionGranted(permission)) {
            permissionsCallback.onPermissionRequestResult(requestCode, true);
        } else {
            requestCodeToCallbackMap.put(requestCode, permissionsCallback);
            if (shouldShowRequestPermissionRationaleDialog(permission)) {
                showOpenSettingDialog(requestCode);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
            SharedPrefsManager.getInstance().setBoolean(permission, true);
        }
    }

    private void showOpenSettingDialog(final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.open_setting_screen)
                .setPositiveButton(R.string.open, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openAppSettingScreen();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onRequestPermissionsResult(requestCode, false);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void openAppSettingScreen() {
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + this.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        this.startActivity(i);
    }

    public boolean checkPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public void unregisterForPermissionRequest(int requestCode) {
        requestCodeToCallbackMap.remove(requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        onRequestPermissionsResult(requestCode, grantResults[0] ==
                PackageManager.PERMISSION_GRANTED);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onRequestPermissionsResult(int requestCode, boolean isGranded) {
        IPermissionsCallback permissionsCallback = requestCodeToCallbackMap.get(requestCode);
        if (permissionsCallback != null) {
            unregisterForPermissionRequest(requestCode);
            permissionsCallback.onPermissionRequestResult(requestCode, isGranded);
        }
    }

    public boolean shouldShowRequestPermissionRationaleDialog(String permission) {
        if (checkPermissionGranted(permission)) {
            return false;
        }
        boolean haveAskedBefore = SharedPrefsManager.getInstance().getBoolean(permission);
        boolean showRationale = ActivityCompat
                .shouldShowRequestPermissionRationale(this, permission);

        return haveAskedBefore && !showRationale;
        //return showRationale;
    }

}
