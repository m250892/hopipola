package com.example.manoj.utilshelper.core;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by manoj on 14/02/16.
 */
public class GsonUtils {

    public static String createJSONStringFromObject(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static <T> T createObjectFromJSONString(String jsonString, Class<T> clazz)
            throws JsonSyntaxException {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, clazz);
    }

}
