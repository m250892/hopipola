package com.example.manoj.utilshelper.managers;

import org.greenrobot.eventbus.EventBus;

public class EventManager {

    private EventBus myEventBus;

    private EventManager() {
        this.myEventBus = EventBus.getDefault();
    }

    private EventBus getMyEventBus() {
        return myEventBus;
    }


    private static class InstanceHolder {
        private static final EventManager INSTANCE = new EventManager();
    }

    public static EventManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public static EventBus getDefaultEventBus() {
        return getInstance().getMyEventBus();
    }


}