package com.example.manoj.utilshelper.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.manoj.utilshelper.core.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 19/02/16.
 */
public abstract class BaseListAdapter<M> extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<M> dataSet;

    public BaseListAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BaseListAdapter(Context context, List<M> dataSet) {
        this(context);
        this.dataSet = dataSet;
        if (this.dataSet == null) {
            this.dataSet = new ArrayList<>();
        }
    }


    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public M getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    protected Context getContext() {
        return context;
    }

    protected LayoutInflater getInflater() {
        return inflater;
    }

    protected View getLayoutView(int id) {
        return getInflater().inflate(id, null);
    }

    protected void setTextView(TextView textView, String data) {
        setTextView(textView, data, true);
    }

    protected void setTextView(TextView textView, String data, boolean hideViewOnWrongData) {
        if (StringUtils.isEmpty(data)) {
            if (hideViewOnWrongData) textView.setVisibility(View.GONE);
        } else {
            if (textView.getVisibility() == View.GONE) textView.setVisibility(View.VISIBLE);
            textView.setText(data);
        }
    }
}
