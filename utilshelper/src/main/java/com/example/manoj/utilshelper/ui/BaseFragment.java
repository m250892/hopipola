package com.example.manoj.utilshelper.ui;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.manoj.utilshelper.Utils;


/**
 * Created by manoj on 14/02/16.
 */
public abstract class BaseFragment extends BaseSingleActivityFragment {

    protected Activity activity;
    private IBaseFragmentController baseFragmentControllerActivity;

    public void setToolbar(Toolbar toolbar) {
        getFragmentController().setSupportActionBarM(toolbar);
        getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(false);
        getFragmentController().getSupportActionBarM().setDisplayHomeAsUpEnabled(true);
    }

    public void setToolbarTitle(String toolbarTitle) {
        getFragmentController().getSupportActionBarM().setTitle(toolbarTitle);
        getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentController().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = activity;
            baseFragmentControllerActivity = (IBaseFragmentController) activity;
        } catch (ClassCastException cce) {
            throw new ClassCastException(
                    getName() + " Fragment's parent activity: " + activity.toString() +
                            "must implement IFragmentController");
        }
    }

    public String getName() {
        return this.getClass().getCanonicalName();
    }

    public IBaseFragmentController getFragmentController() {
        if (baseFragmentControllerActivity == null) {
            baseFragmentControllerActivity = (IBaseFragmentController) getActivity();
        }
        return baseFragmentControllerActivity;
    }

    protected Activity getActivityReference() {
        if (activity == null) {
            activity = getActivity();
        }
        return activity;
    }

    @Override
    public void onDetach() {
        this.activity = null;
        this.baseFragmentControllerActivity = null;
        super.onDetach();
    }

    public void hideKeyboard() {
        View view = getActivityReference().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivityReference().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onDestroyView() {
        hideKeyboard();
        super.onDestroyView();
    }


    public void onLoginSuccess() {
        Utils.log.d("Login Success");
    }
}
