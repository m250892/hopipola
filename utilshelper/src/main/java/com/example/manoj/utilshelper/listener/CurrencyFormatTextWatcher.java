package com.example.manoj.utilshelper.listener;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.manoj.utilshelper.Utils;

import java.text.DecimalFormat;
import java.text.ParseException;

public class CurrencyFormatTextWatcher implements TextWatcher {

    private DecimalFormat df;
    private DecimalFormat dfnd;
    private boolean hasFractionalPart;

    private EditText et;
    String rupeeSymbol = "\\u20B9";

    public CurrencyFormatTextWatcher(EditText et) {
        df = new DecimalFormat("#,###.##");
        df.setDecimalSeparatorAlwaysShown(true);
        dfnd = new DecimalFormat("##,##,##0");
        this.et = et;
        hasFractionalPart = false;
    }


    @Override
    public void afterTextChanged(Editable s) {
        et.removeTextChangedListener(this);

        try {
            int inilen, endlen;
            inilen = et.getText().length();
            String v = s.toString().replace(
                    String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
            String str = v.replace(rupeeSymbol, "");

            Number n = df.parse(str);
            int cp = et.getSelectionStart();
            if (hasFractionalPart) {
                et.setText(rupeeSymbol + df.format(n));
            } else {
                et.setText(rupeeSymbol + dfnd.format(n));
            }
            endlen = et.getText().length();
            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= et.getText().length()) {
                et.setSelection(sel);
            } else {
                et.setSelection(et.getText().length() - 1);
            }
        } catch (NumberFormatException nfe) {
            Utils.log.e("number formate exception" + nfe);
        } catch (ParseException e) {
            Utils.log.e("parser exception" + e);
        }

        if (et.getText().length() == 0) {
            et.setText(rupeeSymbol);
        }
        if (et.getText() != null && et.getText().length() == 1) {
            et.setSelection(et.getText().length());
        }
        et.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString()
                .contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()))) {
            hasFractionalPart = true;
        } else {
            hasFractionalPart = false;
        }
    }

}
