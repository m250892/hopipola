package com.example.manoj.utilshelper.ui;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.example.manoj.utilshelper.core.AppUtils;
import com.example.manoj.utilshelper.location.CustomLocationManager;
import com.example.manoj.utilshelper.location.LocationChangeListener;

public abstract class BaseLocationActivity extends BaseActivity implements CustomLocationManager.LocationManagerCallback {
    private LocationChangeListener locationChangeListener;
    protected CustomLocationManager customLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customLocationManager = new CustomLocationManager(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (customLocationManager != null) {
            customLocationManager.connectGoogleApiClient();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == customLocationManager.REQUEST_RESOLVE_ERROR) {
            customLocationManager.mResolvingError = false;
            if (resultCode == RESULT_OK) {
                customLocationManager.connectGoogleApiClient();
            }
        }
    }

    public boolean fetchCurrentLocation(LocationChangeListener callback) {
        if (AppUtils.isGPSEnabled()) {
            locationChangeListener = callback;
            customLocationManager.startLocationUpdates();
            return true;
        } else {
            callback.onLocationChange(null);
            showSettingsAlert();
            return false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationChangeListener != null) {
            locationChangeListener.onLocationChange(location);
            locationChangeListener = null;
        }
    }

    public void showSettingsAlert() {
        AppUtils.openSettingDialog();
    }

    public void unregisterLocationChangeListenerCallback() {
        locationChangeListener = null;
    }

    @Override
    protected void onStop() {
        if (customLocationManager != null) {
            customLocationManager.disconnectGoogleApiClient();
        }
        super.onStop();
    }
}
