package com.example.manoj.utilshelper.interfaces;

public interface IPermissionsCallback {
    int GET_ACCOUNTS = 1;
    int GET_FINE_LOCATION = 2;
    int GET_CORE_LOCATION = 3;
    int READ_CONTACTS = 5;
    int READ_SMS = 6;
    int RECEIVE_SMS = 7;


    void onPermissionRequestResult(int requestCode, boolean result);
}