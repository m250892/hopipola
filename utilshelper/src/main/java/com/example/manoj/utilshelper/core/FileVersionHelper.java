package com.example.manoj.utilshelper.core;

import android.content.Context;

import com.example.manoj.utilshelper.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileVersionHelper {

    private Context context;

    public FileVersionHelper() {
        this.context = Utils.getContext();
    }

    public boolean isFileExist(String filename) {
        File file = context.getFileStreamPath(filename);
        return file.exists();
    }

    public boolean writeDataInFile(String filename, String data) {
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getDataFromFile(String filename) {
        Utils.log.d("Fetching data from internal storage for : " + filename);
        FileInputStream inputStream = null;
        try {
            inputStream = context.openFileInput(filename);
            String result = readDataFromInputStream(inputStream);
            inputStream.close();
            return result;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String readDataFromInputStream(InputStream inputStream) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                total.append(line);
            }
            return total.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getLatestFile(String filename) {
        Utils.log.d(filename + " : data fetched called");
        if (isFileExist(filename)) {
            return getDataFromFile(filename);
        } else {
            Utils.log.d("Fetching default data for : " + filename);
            return null;
        }
    }

    public void updateData(String filename, Object data) {
        Utils.log.d(filename + " : data Updated in Internal Storage");
        String stringData = Utils.gson.createJSONStringFromObject(data);
        writeDataInFile(filename, stringData);
    }

    public <M> M getModel(String filename, Class<M> className) {
        String data = getLatestFile(filename);
        return Utils.gson.createObjectFromJSONString(data, className);
    }
}
