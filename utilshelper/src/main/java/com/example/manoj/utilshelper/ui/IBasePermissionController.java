package com.example.manoj.utilshelper.ui;

import com.example.manoj.utilshelper.interfaces.IPermissionsCallback;

/**
 * Created by manoj on 09/06/16.
 */
public interface IBasePermissionController {
    void requestPermission(int requestCode, IPermissionsCallback permissionsCallback);

    void unregisterForPermissionRequest(int requestCode);
}

