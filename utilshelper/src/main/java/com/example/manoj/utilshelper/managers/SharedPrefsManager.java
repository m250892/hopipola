package com.example.manoj.utilshelper.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.model.Model;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefsManager {
    private SharedPreferences sharedPreferences;
    private static SharedPrefsManager instance;
    public static final String PREF_NAME = "packagename";

    private SharedPrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPrefsManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(
                    "SharedPrefsManager is not initialized, call initialize(applicationContext) static method first");
        }
        return instance;
    }

    public static void initialize(Context context) {
        if (context == null) {
            throw new NullPointerException("Context is null");
        }
        if (instance == null) {
            synchronized (SharedPrefsManager.class) {
                if (instance == null) {
                    instance = new SharedPrefsManager(context);
                }
            }
        }
    }

    private SharedPreferences getPrefs() {
        return sharedPreferences;
    }

    public void clearPrefs() {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.clear();
        editor.commit();
    }

    public void removeKey(String key) {
        getPrefs().edit().remove(key).commit();
    }

    public boolean containsKey(String key) {
        return getPrefs().contains(key);
    }

    public String getString(String key, String defValue) {
        return getPrefs().getString(key, defValue);
    }

    public String getString(String key) {
        return getString(key, null);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public int getInt(String key, int defValue) {
        return getPrefs().getInt(key, defValue);
    }

    public int getInt(String key) {
        return getInt(key, 0);
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public long getLong(String key, long defValue) {
        return getPrefs().getLong(key, defValue);
    }

    public long getLong(String key) {
        return getLong(key, 0L);
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return getPrefs().getBoolean(key, defValue);
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getFloat(String key) {
        return getFloat(key, 0f);
    }

    public boolean getFloat(String key, float defValue) {
        return getFloat(key, defValue);
    }

    public void setFloat(String key, Float value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public <M extends Model> void setObjectList(String key, List<M> data) {
        String value = Utils.gson.createJSONStringFromObject(data);
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public <M extends Model> List<M> getObjectList(String key, Class<M> classOfModelObject) {
        String jsonData = getPrefs().getString(key, null);
        if (null != jsonData) {
            try {
                Gson gson = new Gson();
                Type type = new TypeToken<List<M>>() {
                }.getType();

                List<M> data = gson.fromJson(jsonData, type);

                List<M> result = new ArrayList<>();
                result.addAll(data);
                return result;
            } catch (ClassCastException cce) {
                Utils.log.e("Cannot convert string obtained from prefs into collection of type " +
                        classOfModelObject.getName());
            }
        }
        return null;
    }

    public <M extends Model> void setObject(String key, M modelObject) {
        String value = Utils.gson.createJSONStringFromObject(modelObject);
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public <M extends Model> M getObject(String key, Class<M> classOfModelObject) {
        String jsonData = getPrefs().getString(key, null);
        if (null != jsonData) {
            try {
                Gson gson = new Gson();
                M customObject = gson.fromJson(jsonData, classOfModelObject);
                return customObject;
            } catch (ClassCastException cce) {
                Utils.log.e("Cannot convert string obtained from prefs into collection of type " +
                        classOfModelObject.getName());
            }
        }
        return null;
    }

    public SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }
}