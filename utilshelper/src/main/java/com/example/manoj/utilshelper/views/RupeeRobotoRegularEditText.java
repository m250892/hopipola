package com.example.manoj.utilshelper.views;

import android.content.Context;
import android.util.AttributeSet;

import com.example.manoj.utilshelper.listener.CurrencyFormatTextWatcher;


/**
 * Created by manoj on 03/02/16.
 */
public class RupeeRobotoRegularEditText extends RobotoRegularEditText {

    public RupeeRobotoRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RupeeRobotoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RupeeRobotoRegularEditText(Context context) {
        super(context);
    }

    {
        addTextChangedListener(new CurrencyFormatTextWatcher(this));
    }


    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (selStart == 0 && getText().length() > 0) {
            setSelection(1, Math.max(1, selEnd));
        }
    }
}
