package cleanslatetechnologies.com.hoppiapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.example.manoj.utilshelper.interfaces.IPermissionsCallback;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseSingleActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import cleanslatetechnologies.com.hoppiapp.homescreen.HomeActivity;
import cleanslatetechnologies.com.hoppiapp.homescreen.HomeDataStore;
import cleanslatetechnologies.com.hoppiapp.models.HomePageDataModel;
import cleanslatetechnologies.com.hoppiapp.models.UpdateFID;
import cleanslatetechnologies.com.hoppiapp.onboarding.OnBoardingActivity;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

public class SplashActivity extends BaseSingleActivity implements LocationListener, IPermissionsCallback {

    private static final int GPS_DISTANCE = 200;
    @Bind(R.id.progressBar)
    View progressBar;

    protected LocationManager locationManager;
    private Location location;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (location != null) {
            fetchLocationSuccess();
        } else {
            requestLocationUpdate();
        }
    }

    public void fetchLocationSuccess() {
        if (UserProfileManager.getUserDetail().isLoggedIn()) {
            fetchHomeData();
        } else {
            Intent intent = new Intent(this, OnBoardingActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.PARAM_LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            fetchHomeData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void fetchHomeData() {
        progressBar.setVisibility(View.VISIBLE);
        Rest.connect().GET().load(getUrl()).as(HomePageDataModel.class).withCallback(new RequestCallback<HomePageDataModel>() {
            @Override
            public void onRequestSuccess(HomePageDataModel data) {
                if (data == null) {
                    return;
                }
                HomeDataStore.getInstance().setHomeData(data);
                progressBar.setVisibility(View.GONE);
                checkForFirebaseIDUpdate();
                Log.d("manoj", "onHomeData load success");
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onRequestError(RequestError error) {
                progressBar.setVisibility(View.GONE);
                finish();
                Log.d("manoj", "onHomeData load failed");
            }
        });
    }

    private void checkForFirebaseIDUpdate() {
        String currentFID = UserProfileManager.getInstance().getFireBaseId();
        if (currentFID != null && !currentFID.equalsIgnoreCase(HomeDataStore.getInstance().getHomeData().getFirebaseID())) {
            UpdateFID updateFID = new UpdateFID(UserProfileManager.getUserDetail().getPhoneNumber(), currentFID);
            Log.d("manoj", updateFID.getJsonObj().toString());
            Rest.connect().POST(updateFID.getJsonObj()).load(Constants.UPDATE_FIREBASE_ID).as(Model.class).withCallback(new RequestCallback() {
                @Override
                public void onRequestSuccess(Object o) {
                }

                @Override
                public void onRequestError(RequestError error) {
                }
            });
        }
    }


    public String getUrl() {
        Uri.Builder uri = Uri.parse(Constants.HOME_URL).buildUpon();
        uri.appendQueryParameter("user", UserProfileManager.getUserDetail().getPhoneNumber());
        uri.appendQueryParameter("lat", String.valueOf(location.getLatitude()));
        uri.appendQueryParameter("long", String.valueOf(location.getLongitude()));
        return uri.build().toString();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (this.location == null && location != null) {
            progressBar.setVisibility(View.GONE);
            removeLocationUpdates();
            this.location = location;
            fetchLocationSuccess();
        }
    }

    private void requestLocationUpdate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (!checkPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    requestPermission(IPermissionsCallback.GET_FINE_LOCATION, this);
                } else {
                    requestPermission(IPermissionsCallback.GET_CORE_LOCATION, this);
                }
            } else {
                progressBar.setVisibility(View.VISIBLE);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    onLocationChanged(location);
                } else {
                    //fetch new location
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, GPS_DISTANCE, this);
                }

            }
        } else {
            progressBar.setVisibility(View.VISIBLE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                onLocationChanged(location);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, GPS_DISTANCE, this);
            }
        }
    }

    private void removeLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locationManager.removeUpdates(this);
            }
        } else {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        requestLocationUpdate();
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (location == null) {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (locationManager != null) {
            removeLocationUpdates();
        }
    }

    @Override
    public void onPermissionRequestResult(int requestCode, boolean result) {
        if (result) {
            requestLocationUpdate();
        }
    }
}
