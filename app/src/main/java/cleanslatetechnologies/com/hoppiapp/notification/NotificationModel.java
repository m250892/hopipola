package cleanslatetechnologies.com.hoppiapp.notification;

/**
 * Created by manoj on 16/06/16.
 */
public class NotificationModel {
    private String number;
    private String body;
    private String type;
    private String names;
    private String message;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return body + ", " + names + " " + message;
    }

    public String getTitle() {
        return "Hopipola";
    }
}
