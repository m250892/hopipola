package cleanslatetechnologies.com.hoppiapp.notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.manoj.utilshelper.core.GsonUtils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

public class NotificationService extends IntentService {

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NotificationUtils.ID_NOTIFICATION);

        String senderNumber = intent.getStringExtra(NotificationUtils.SENDER_NUMBER);
        String action = intent.getStringExtra(NotificationUtils.ACTION);
        int responseType = -1;
        switch (action) {
            case NotificationUtils.ACTION_YES:
                responseType = 0;
                break;
            case NotificationUtils.ACTION_NO:
                responseType = 1;
                break;
            default:
                break;
        }

        CombineResponseFormat combineResponseFormat = new CombineResponseFormat(senderNumber, responseType);
        Log.d("manoj", "Response format " + combineResponseFormat.getJsonObj().toString());
        Rest.connect().POST(combineResponseFormat.getJsonObj()).load(Constants.COMBINE_INVITE_RESPONSE).as(Model.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                //success
            }

            @Override
            public void onRequestError(RequestError error) {
                //failed
            }
        });
    }

    private class CombineResponseFormat {
        @SerializedName("my_number")
        private String userNumber;
        @SerializedName("number")
        private String senderNumber;
        @SerializedName("response")
        private int type;

        public CombineResponseFormat(String senderNumber, int type) {
            this.userNumber = UserProfileManager.getUserDetail().getPhoneNumber();
            this.senderNumber = senderNumber;
            this.type = type;
        }

        public JSONObject getJsonObj() {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(GsonUtils.createJSONStringFromObject(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }
    }
}
