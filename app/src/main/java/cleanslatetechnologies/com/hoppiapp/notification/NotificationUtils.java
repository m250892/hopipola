package cleanslatetechnologies.com.hoppiapp.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.SplashActivity;

/**
 * Created by manoj on 16/06/16.
 */
public class NotificationUtils {

    public static final int ID_NOTIFICATION = 0;
    public static final int ID_DEFAULT_NOTIFICATION = 1;
    public static final int ID_COMBINE_NOTIFICATION = 2;
    private static final int ID_PENDING_INTENT_DEFAULT = 11;
    private static final int ID_PENDING_INTENT_YES = 12;
    private static final int ID_PENDING_INTENT_NO = 13;

    public static final String ACTION = "action";
    public static final String ACTION_YES = "action_yes";
    public static final String ACTION_NO = "action_no";
    public static final String SENDER_NUMBER = "sender_number";
    private static final String EXTRA_MESSAGE = "extra_msg";

    public static void combineRequestNotification(Context context, NotificationModel data) {
        /*
         * Action for clicking the notification itself (not one of the buttons).
         * This will be set as 'default' action with setContentIntent(), so it does
         * not have to be wrapped in an Action object.
         */
        Intent resultIntent = new Intent(context, NotificationService.class);
        resultIntent.putExtra(EXTRA_MESSAGE, data.getDescription());

        PendingIntent pendingIntentDefault = PendingIntent.getActivity(
                context,
                ID_PENDING_INTENT_DEFAULT,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        /*
         * Action for clicking yes.
         */
        Intent intentYes = new Intent(context, NotificationService.class)
                .putExtra(ACTION, ACTION_YES)
                .putExtra(SENDER_NUMBER, data.getNumber());
        PendingIntent pendingIntentYes = PendingIntent.getService(
                context,
                ID_PENDING_INTENT_YES,
                intentYes,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Action actionYes = new NotificationCompat.Action(
                android.R.drawable.ic_input_add,
                context.getString(R.string.yes),
                pendingIntentYes
        );

        /*
         * Action for clicking no.
         */
        Intent intentNo = new Intent(context, NotificationService.class)
                .putExtra(ACTION, ACTION_NO)
                .putExtra(SENDER_NUMBER, data.getNumber());
        PendingIntent pendingIntentNo = PendingIntent.getService(
                context,
                ID_PENDING_INTENT_NO,
                intentNo,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Action actionNo = new NotificationCompat.Action(
                android.R.drawable.ic_delete,
                context.getString(R.string.no),
                pendingIntentNo
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(data.getTitle())
                .setContentText(data.getDescription())
                .setAutoCancel(true)
                .setContentIntent(pendingIntentDefault)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(data.getDescription()))
                .addAction(actionNo)
                .addAction(actionYes);

        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_COMBINE_NOTIFICATION, notification);
    }


    public static void buildNotification(Context context, NotificationModel data) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setContentTitle(data.getTitle())
                .setContentText(data.getDescription())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(ID_DEFAULT_NOTIFICATION, notificationBuilder.build());
    }


    public static void sendNotification(Context context, NotificationModel notificationModel) {
        //type 0 for combine request
        if (notificationModel.getType().equals("0")) {
            NotificationUtils.combineRequestNotification(context, notificationModel);
        } else {
            NotificationUtils.buildNotification(context, notificationModel);
        }
    }
}