package cleanslatetechnologies.com.hoppiapp.models;

import com.example.manoj.utilshelper.model.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 10/06/16.
 */
public class ContactDataModel extends Model implements Serializable {

    private String name;
    private String phoneNumber;
    private List<String> email;


    public ContactDataModel() {
        this(null, null, new ArrayList<String>());
    }

    public ContactDataModel(String name, String phoneNumber, List<String> email) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "Name : " + name + ", phone : " + phoneNumber + ", email : " + email.toString();
    }
}
