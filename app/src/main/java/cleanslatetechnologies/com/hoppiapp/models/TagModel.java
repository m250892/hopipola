package cleanslatetechnologies.com.hoppiapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 28/06/16.
 */
public class TagModel {

    @SerializedName("tag")
    private String name;
    private int score;

    public TagModel(String lastTag) {
        this.name = lastTag;
        this.score = 0;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }
}
