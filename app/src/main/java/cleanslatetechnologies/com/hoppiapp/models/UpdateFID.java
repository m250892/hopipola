package cleanslatetechnologies.com.hoppiapp.models;

import com.example.manoj.utilshelper.core.GsonUtils;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manoj on 28/06/16.
 */
public class UpdateFID {
    @SerializedName("user")
    private String number;
    @SerializedName("fid")
    private String FirebaseID;

    public UpdateFID(String number, String firebaseID) {
        this.number = number;
        FirebaseID = firebaseID;
    }

    public JSONObject getJsonObj() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(GsonUtils.createJSONStringFromObject(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String getNumber() {
        return number;
    }

    public String getFirebaseID() {
        return FirebaseID;
    }
}
