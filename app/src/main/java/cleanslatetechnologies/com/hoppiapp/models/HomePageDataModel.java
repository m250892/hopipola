package cleanslatetechnologies.com.hoppiapp.models;

import com.example.manoj.utilshelper.model.Model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by manoj on 13/06/16.
 */
public class HomePageDataModel extends Model implements Serializable {

    @SerializedName("tags")
    private List<TagModel> userTags;
    private String distance;
    private String time;
    @SerializedName("store_location")
    private String address;
    @SerializedName("store_location_lat")
    private String locationLat;
    @SerializedName("store_location_long")
    private String locationLong;
    @SerializedName("store_location_opentime")
    private String openTime;
    @SerializedName("store_location_closetime")
    private String closeTime;
    @SerializedName("store_location_rating")
    private String rating;
    @SerializedName("points")
    private String userPoints;

    @SerializedName("appFlag")
    private int appFlag;

    @SerializedName("contactFlag")
    private int contactFlag;

    @SerializedName("count_referrals")
    private String unlockDishCount;

    @SerializedName("count_rewards")
    private String lockDishCount;

    @SerializedName("firebaseID")
    private String firebaseID;

    public HomePageDataModel() {
    }

    public List<TagModel> getUserTags() {
        return userTags;
    }

    public void setUserTags(List<TagModel> userTags) {
        this.userTags = userTags;
    }

    public String getDistance() {
        return distance;
    }

    public String getTime() {
        return time;
    }

    public String getAddress() {
        return address;
    }

    public String getLocationLat() {
        return locationLat;
    }

    public String getLocationLong() {
        return locationLong;
    }

    public String getOpenTime() {
        return openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public String getRating() {
        return rating;
    }

    public String getUserPoints() {
        return userPoints;
    }

    public String getTimeingText() {
        return openTime + " - " + closeTime;
    }

    public String getUnlockDishCount() {
        return unlockDishCount;
    }

    public String getLockDishCount() {
        return lockDishCount;
    }

    public String getFirebaseID() {
        return firebaseID;
    }
}
