package cleanslatetechnologies.com.hoppiapp.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.login.SignUpActivity;

public class OnBoardingActivity extends AppCompatActivity {

    private ViewPager viewPager;

    private View firstStep, secondStep, thirdStep, fourthStep;

    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private OnBoardingAdapter pagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        ButterKnife.bind(this);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        List<Integer> img = new ArrayList<>();
        img.add(R.drawable.first_step_on_boarding_bg);
        img.add(R.drawable.second_step_on_boarding_bg);
        img.add(R.drawable.thrid_step_on_boarding_bg);
        img.add(R.drawable.login_bg);
        pagerAdapter = new OnBoardingAdapter(getSupportFragmentManager(), img);
        viewPager.setAdapter(pagerAdapter);
        firstStep = findViewById(R.id.first_step);
        secondStep = findViewById(R.id.second_step);
        thirdStep = findViewById(R.id.third_step);
        fourthStep = findViewById(R.id.fourth_step);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        setUiPageViewController();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                onPageChanged(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        onPageChanged(0);
    }

    private void setUiPageViewController() {
        dotsCount = pagerAdapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.not_selected_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            int margin = getResources().getDimensionPixelSize(R.dimen.small);
            params.setMargins(margin, 0, margin, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[viewPager.getCurrentItem()].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));
    }


    private void onPageChanged(int position) {
        hideAll();
        switch (position) {
            case 0:
                firstStep.setVisibility(View.VISIBLE);
                break;
            case 1:
                secondStep.setVisibility(View.VISIBLE);
                break;
            case 2:
                thirdStep.setVisibility(View.VISIBLE);
                break;
            case 3:
                fourthStep.setVisibility(View.VISIBLE);
                break;
        }

        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.not_selected_item_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));
    }

    private void hideAll() {
        firstStep.setVisibility(View.GONE);
        secondStep.setVisibility(View.GONE);
        thirdStep.setVisibility(View.GONE);
        fourthStep.setVisibility(View.GONE);
    }

    @OnClick(R.id.first_step)
    public void onGetStartedClick() {
        viewPager.setCurrentItem(1);
    }

    @OnClick(R.id.second_step_skip)
    public void onSkipIntroClick() {
        viewPager.setCurrentItem(3);
    }

    @OnClick(R.id.second_step_next)
    public void onSecondNextClick() {
        viewPager.setCurrentItem(2);
    }

    @OnClick(R.id.third_step)
    public void onThirdNextClick() {
        viewPager.setCurrentItem(3);
    }

    @OnClick(R.id.fourth_step_login)
    public void onLoginClick() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.fourth_step_signup)
    public void onSignUpClick() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra(SignUpActivity.KEY_SOURCE, true);
        startActivity(intent);
        finish();
    }
}
