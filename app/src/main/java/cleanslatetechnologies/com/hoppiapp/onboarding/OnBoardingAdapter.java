package cleanslatetechnologies.com.hoppiapp.onboarding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by manoj on 21/06/16.
 */
public class OnBoardingAdapter extends FragmentStatePagerAdapter {

    private List<Integer> drawableIds;

    public OnBoardingAdapter(FragmentManager fm, List<Integer> drawableIds) {
        super(fm);
        this.drawableIds = drawableIds;
    }

    @Override
    public Fragment getItem(int position) {
        return OnBoardingView.newInstance(drawableIds.get(position));
    }

    @Override
    public int getCount() {
        return drawableIds.size();
    }
}
