package cleanslatetechnologies.com.hoppiapp.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import cleanslatetechnologies.com.hoppiapp.R;

public class OnBoardingView extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private int resId;

    public static OnBoardingView newInstance(int resID) {
        OnBoardingView fragment = new OnBoardingView();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            resId = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View baseView = inflater.inflate(R.layout.fragment_on_boarding_view, container, false);
        ImageView imageView = (ImageView) baseView.findViewById(R.id.image_view);
        if (resId > 0) {
            imageView.setBackgroundResource(resId);
        }
        return baseView;
    }

}
