package cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;

/**
 * Created by manoj on 17/06/16.
 */
public class WaitingListAdapter extends RecyclerView.Adapter<WaitingListAdapter.ViewHolder> {

    private Context context;
    private List<WaitingFriendModel> dataList;

    public WaitingListAdapter(Context context, List<WaitingFriendModel> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View baseView = LayoutInflater.from(context).inflate(R.layout.layout_waiting_friend_view, null);
        return new ViewHolder(baseView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WaitingFriendModel data = dataList.get(position);

        holder.nameView.setText(data.getName());
        holder.numberView.setText(data.getPhoneNumber());
        String statusText;
        int colorValue;
        switch (data.getStatus()) {
            case 0:
                statusText = "ACCEPTED";
                colorValue = R.color.accepted_color;
                break;
            case 1:
                statusText = "DECLINED";
                colorValue = R.color.red;
                break;
            case 2:
                statusText = "NO RESPONSE";
                colorValue = R.color.black;
                break;
            default:
                statusText = "Waiting for response";
                colorValue = R.color.secondary_text;
        }
        holder.statusView.setText(statusText);
        holder.statusView.setTextColor(ContextCompat.getColor(context, colorValue));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameView, numberView;
        TextView statusView;

        public ViewHolder(View itemView) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(R.id.name_view);
            numberView = (TextView) itemView.findViewById(R.id.number_view);
            statusView = (TextView) itemView.findViewById(R.id.status_text);
        }
    }
}
