package cleanslatetechnologies.com.hoppiapp.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.manoj.utilshelper.ui.BaseDialog;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward.FragmentBaseRewards;
import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward.RewardDataModel;

/**
 * Created by manoj on 15/06/16.
 */
public class ConfirmRewardDialog extends BaseDialog implements SeekBar.OnSeekBarChangeListener {

    private static final String KEY_DATA = "reqard_data";
    private TextView dishname;
    private ImageView dishtypeIcon;
    private RewardDataModel rewardData;

    public static ConfirmRewardDialog newInstance(RewardDataModel data) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, data);
        ConfirmRewardDialog fragment = new ConfirmRewardDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rewardData = (RewardDataModel) getArguments().getSerializable(KEY_DATA);
    }

    @Override
    protected void initViews(View baseView) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dishtypeIcon = (ImageView) baseView.findViewById(R.id.dish_type_icon);
        dishname = (TextView) baseView.findViewById(R.id.item_name);
        baseView.findViewById(R.id.back_btn).setOnClickListener(this);
        SeekBar seekBar = (SeekBar) baseView.findViewById(R.id.confirm_seek_bar);
        seekBar.setOnSeekBarChangeListener(this);

        dishname.setText(rewardData.getNames());
        dishtypeIcon.setImageResource(rewardData.getCategory().getDrawableId());

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_confirm_treat_layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                dismiss();
                break;
        }
        super.onClick(v);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress >= 100) {
            confirmTreat();
        }
    }

    private void confirmTreat() {
        Log.d("manoj", "Treat confirm");
        Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        if (getParentFragment() instanceof FragmentBaseRewards) {
            ((FragmentBaseRewards) getParentFragment()).onTreatSelected(rewardData);
        }
        dismiss();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
