package cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse;

import java.io.Serializable;

/**
 * Created by manoj on 17/06/16.
 */
public class WaitingFriendModel implements Serializable {
    private String name;
    private String phoneNumber;
    private int status = -1;  // -1 for waiting, 0-> accept, 1->reject, 2 -> not respond

    public WaitingFriendModel(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
