package cleanslatetechnologies.com.hoppiapp.dialogs;

import android.content.Intent;
import android.view.View;

import com.example.manoj.utilshelper.ui.BaseDialog;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.ContactCombineActivity;

/**
 * Created by manoj on 15/06/16.
 */
public class CombineEmptyDialog extends BaseDialog {
    @Override
    protected void initViews(View baseView) {
        baseView.findViewById(R.id.fabEmptyCombine).setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_combine_text_layout;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fabEmptyCombine) {
            handleCombineBtnClick();
        }
        super.onClick(v);

    }

    private void handleCombineBtnClick() {
        Intent intent = new Intent(getContext(), ContactCombineActivity.class);
        getActivity().startActivityForResult(intent, Constants.PARAM_COMBINE_RESULT_CODE);
        dismiss();
    }
}
