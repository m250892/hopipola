package cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse;

/**
 * Created by manoj on 17/06/16.
 */
public class FriendResponseEvent {
    private String number;
    private int status;

    public FriendResponseEvent(String number, int status) {
        this.number = number;
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public int getStatus() {
        return status;
    }
}
