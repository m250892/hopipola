package cleanslatetechnologies.com.hoppiapp.dialogs;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.Toast;

import com.example.manoj.utilshelper.ui.BaseDialog;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.ReferFriendsActivity;

/**
 * Created by manoj on 13/06/16.
 */
public class AppReferDialog extends BaseDialog {


    @Override
    protected void initViews(View baseView) {
        baseView.findViewById(R.id.back_btn).setOnClickListener(this);
        baseView.findViewById(R.id.medium_sms).setOnClickListener(this);
        baseView.findViewById(R.id.medium_whatsapp).setOnClickListener(this);
        baseView.findViewById(R.id.medium_messanger).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                dismiss();
                break;
            case R.id.medium_sms:
                handleInviteViaSms();
                break;
            case R.id.medium_whatsapp:
                handleInviteViaWhatsapp();
                break;
            case R.id.medium_messanger:
                handleInviteViaMessanger();
                break;
        }
        super.onClick(v);
    }

    private void handleInviteViaSms() {
        Intent contactSelection = new Intent(getContext(), ReferFriendsActivity.class);
        startActivity(contactSelection);
        dismiss();
    }

    private void handleInviteViaWhatsapp() {
        if (appInstalled("com.whatsapp")) {
            Intent iWhatsapp = new Intent();
            iWhatsapp.setAction(Intent.ACTION_SEND);
            iWhatsapp.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            iWhatsapp.setType("text/plain");
            iWhatsapp.setPackage("com.whatsapp");
            startActivity(iWhatsapp);
            dismiss();
        } else {
            showToast("Whatsapp not installed on your phone.");
        }
    }

    private void showToast(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    private boolean appInstalled(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void handleInviteViaMessanger() {
        if (appInstalled("com.facebook.orca")) {
            Intent iMessenger = new Intent();
            iMessenger.setAction(Intent.ACTION_SEND);
            iMessenger.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            iMessenger.setType("text/plain");
            iMessenger.setPackage("com.facebook.orca");
            startActivity(iMessenger);
            dismiss();
        } else {
            showToast("Messenger not installed on your phone.");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dailog_send_invite_layout;
    }
}
