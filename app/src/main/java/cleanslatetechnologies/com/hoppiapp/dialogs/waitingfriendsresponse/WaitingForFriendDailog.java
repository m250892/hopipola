package cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.utilshelper.managers.EventManager;
import com.example.manoj.utilshelper.ui.BaseDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward.ShareRewardEvent;
import cleanslatetechnologies.com.hoppiapp.utils.CustomTimer;

/**
 * Created by manoj on 17/06/16.
 */
public class WaitingForFriendDailog extends BaseDialog implements CustomTimer.ICountDownTimeCallback {

    private RecyclerView friendsStatusView;
    private List<WaitingFriendModel> dataList;
    private WaitingListAdapter waitingListAdapter;

    public static WaitingForFriendDailog newInstance(CombineRequestResponse data) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.PARAM_COMBINE_FRIENDS_DATA, data);
        WaitingForFriendDailog fragment = new WaitingForFriendDailog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CombineRequestResponse data = (CombineRequestResponse) getArguments().getSerializable(Constants.PARAM_COMBINE_FRIENDS_DATA);
            dataList = data.getWaitingFriendList();
            CustomTimer.getInstance().startTimer(180, 1);
            CustomTimer.getInstance().registerTimerCallback(this);
        }
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
    }


    @Override
    protected void initViews(View baseView) {
        friendsStatusView = (RecyclerView) baseView.findViewById(R.id.firends_status);
        friendsStatusView.setLayoutManager(new LinearLayoutManager(getContext()));
        waitingListAdapter = new WaitingListAdapter(getContext(), dataList);
        friendsStatusView.setAdapter(waitingListAdapter);
        baseView.findViewById(R.id.confirm_btn).setOnClickListener(this);
        baseView.findViewById(R.id.cancel_btn).setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventManager.getDefaultEventBus().register(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        CustomTimer.getInstance().unregisterTimberCallback();
        EventManager.getDefaultEventBus().unregister(this);
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_waiting_for_friend_layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_btn:
                dismiss();
                break;
            case R.id.confirm_btn:
                onConfirmBtnClick();
                break;
        }
        super.onClick(v);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FriendResponseEvent event) {
        onEventReceive(event);
    }

    private void onEventReceive(FriendResponseEvent event) {
        Log.d("manoj", "event reciever : " + event.getNumber() + ", " + event.getStatus());
        if (event != null) {
            for (WaitingFriendModel data : dataList) {
                if (data.getPhoneNumber().equals(event.getNumber())) {
                    data.setStatus(event.getStatus());
                    break;
                }
            }
        }
        waitingListAdapter.notifyDataSetChanged();
    }


    private List<String> getConfirmFirendsNumber() {
        List<String> numbers = new ArrayList<>();
        for (WaitingFriendModel waitingFriendModel : dataList) {
            if (waitingFriendModel.getStatus() == 0) {
                numbers.add(waitingFriendModel.getPhoneNumber());
            }
        }
        return numbers;
    }

    private void onConfirmBtnClick() {
        Log.d("manoj", "onConfirm btn click");
        EventManager.getDefaultEventBus().post(new ShareRewardEvent(getConfirmFirendsNumber()));
        dismiss();
    }

    @Override
    public void onTimerUpdate(long seconds) {

    }

    @Override
    public void onTimerFinish() {
        //change all values
        for (WaitingFriendModel waitingFriendModel : dataList) {
            if (waitingFriendModel.getStatus() == -1) {
                waitingFriendModel.setStatus(2);
            }
        }
        waitingListAdapter.notifyDataSetChanged();
    }
}
