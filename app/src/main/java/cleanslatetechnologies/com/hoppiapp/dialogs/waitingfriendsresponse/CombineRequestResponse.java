package cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by manoj on 18/06/16.
 */
public class CombineRequestResponse implements Serializable {

    private List<WaitingFriendModel> waitingFriendList;

    public CombineRequestResponse(List<WaitingFriendModel> waitingFriendList) {
        this.waitingFriendList = waitingFriendList;
    }

    public List<WaitingFriendModel> getWaitingFriendList() {
        return waitingFriendList;
    }
}
