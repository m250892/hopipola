package cleanslatetechnologies.com.hoppiapp.contactselection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.BaseCSListModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSContactModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSDividerModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSListRowType;

/**
 * Created by manoj on 10/06/16.
 */
public class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactRecyclerAdapter.BaseViewHolder> {

    private Context context;
    private List<BaseCSListModel> filteredList;
    private IOnItemClickListener onItemClickListener;
    private List<BaseCSListModel> originalDataList;

    public ContactRecyclerAdapter(Context context, List<BaseCSListModel> originalDataList) {
        this.context = context;
        this.originalDataList = originalDataList;
        this.filteredList = new ArrayList<>();
        this.filteredList.addAll(originalDataList);
    }

    public BaseCSListModel getObj(int position) {
        return filteredList.get(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = buildViewHolder(CSListRowType.fromValue(viewType));
        return baseViewHolder;
    }

    public void setOnItemClickListener(IOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setFilter(String query) {
        List<BaseCSListModel> newFilterList = new ArrayList<>();
        for (BaseCSListModel dataItem : originalDataList) {
            if (dataItem.keepObjct(query)) {
                newFilterList.add(dataItem);
            }
        }
        filteredList.clear();
        filteredList.addAll(newFilterList);
        notifyDataSetChanged();
    }


    public void onOriginalListChange() {
        this.filteredList.clear();
        this.filteredList.addAll(originalDataList);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder instanceof DividerViewHolder) {
            updateViewData((DividerViewHolder) holder, (CSDividerModel) filteredList.get(position));
        } else if (holder instanceof SelectedContactViewHolder) {
            updateViewData((SelectedContactViewHolder) holder, (CSContactModel) filteredList.get(position));
        } else if (holder instanceof UnSelectedContactViewHolder) {
            updateViewData((UnSelectedContactViewHolder) holder, (CSContactModel) filteredList.get(position));
        } else {
            throw new IllegalStateException("holder type not supported");
        }
    }

    private void updateViewData(DividerViewHolder holder, CSDividerModel data) {
        holder.textView.setText(data.getData());
    }

    private void updateViewData(SelectedContactViewHolder holder, CSContactModel data) {
        holder.userNameView.setText(data.getContactData().getName());
        holder.phoneNumberView.setText(data.getContactData().getPhoneNumber());
    }

    private void updateViewData(UnSelectedContactViewHolder holder, CSContactModel data) {
        updateViewData(holder, data);
    }

    @Override
    public int getItemViewType(int position) {
        return filteredList.get(position).getRowType().ordinal();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {

        private View baseView;

        public BaseViewHolder(View itemView) {
            super(itemView);
            this.baseView = itemView;

        }
    }

    private BaseViewHolder buildViewHolder(CSListRowType rowType) {
        switch (rowType) {
            case DIVIDER:
                View dividerView = LayoutInflater.from(context).inflate(R.layout.layout_divider_contact_selection, null);
                return new DividerViewHolder(dividerView);
            case SELECT:
                View selectView = LayoutInflater.from(context).inflate(R.layout.layout_selected_contact_row, null);
                return new SelectedContactViewHolder(selectView);
            case UNSELECT:
                View unselectView = LayoutInflater.from(context).inflate(R.layout.layout_unselected_contact_row, null);
                return new SelectedContactViewHolder(unselectView);
        }
        return null;
    }

    public class DividerViewHolder extends BaseViewHolder {
        TextView textView;

        public DividerViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.divider_text_view);
        }
    }

    public class SelectedContactViewHolder extends BaseViewHolder implements View.OnClickListener {
        protected TextView userNameView;
        protected TextView phoneNumberView;

        public SelectedContactViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            userNameView = (TextView) itemView.findViewById(R.id.user_name_view);
            phoneNumberView = (TextView) itemView.findViewById(R.id.user_phone_view);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onContactSelected(getAdapterPosition());
            }
        }
    }

    public class UnSelectedContactViewHolder extends SelectedContactViewHolder {

        public UnSelectedContactViewHolder(View itemView) {
            super(itemView);
        }
    }

    interface IOnItemClickListener {
        void onContactSelected(int position);
    }
}
