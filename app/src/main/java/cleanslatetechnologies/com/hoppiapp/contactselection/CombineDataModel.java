package cleanslatetechnologies.com.hoppiapp.contactselection;

import com.example.manoj.utilshelper.core.GsonUtils;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 15/06/16.
 */
public class CombineDataModel {
    @SerializedName("user_number")
    private String userNumber;
    @SerializedName("user_name")
    private String userName;

    @SerializedName("combine")
    private List<String> combine;

    @SerializedName("invite")
    private List<String> invite;

    public CombineDataModel(List<String> combine, List<String> invite) {
        this.userName = UserProfileManager.getUserDetail().getName();
        this.userNumber = UserProfileManager.getUserDetail().getPhoneNumber();
        this.combine = combine;
        this.invite = invite;
    }

    public JSONObject getJsonObj() {
        JSONObject result = null;
        try {
            result = new JSONObject(GsonUtils.createJSONStringFromObject(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

}
