package cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.contactselection.row.BaseCSListModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSContactModel;

/**
 * Created by manoj on 11/06/16.
 */
public abstract class ContactCriteria {

    public List<BaseCSListModel> getContacts(List<BaseCSListModel> csListModels) {
        List<BaseCSListModel> result = new ArrayList<>();
        for (BaseCSListModel data : csListModels) {
            if (data instanceof CSContactModel) {
                CSContactModel tmpObj = (CSContactModel) data;
                if (keepObject(tmpObj)) {
                    result.add(tmpObj);
                }
            }
        }
        Collections.sort(result, new Comparator<BaseCSListModel>() {

            @Override
            public int compare(BaseCSListModel lhs, BaseCSListModel rhs) {
                return compare((CSContactModel) lhs, (CSContactModel) rhs);
            }

            private int compare(CSContactModel lhs, CSContactModel rhs) {
                return lhs.getContactData().getName().compareTo(rhs.getContactData().getName());
            }
        });

        return result;
    }

    protected abstract boolean keepObject(CSContactModel tmpObj);
}
