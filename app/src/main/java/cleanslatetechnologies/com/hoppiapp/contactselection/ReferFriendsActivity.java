package cleanslatetechnologies.com.hoppiapp.contactselection;

import android.util.Log;
import android.view.View;

import com.example.manoj.utilshelper.core.ArrayUtils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.NewUserSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.NewUserUnSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.BaseCSListModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSDividerModel;

public class ReferFriendsActivity extends ContactCombineActivity {

    protected List<BaseCSListModel> buildContactListRow(List<BaseCSListModel> data) {
        List<BaseCSListModel> result = new ArrayList<>();

        List<BaseCSListModel> tmpList = new NewUserSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Invite friends :"));
            result.addAll(tmpList);
            tmpList.clear();
        }

        tmpList = new NewUserUnSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Select friends to invite :"));
            result.addAll(tmpList);
            tmpList.clear();
        }
        return result;
    }

    protected void sendCombineRequest(List<String> combineNumbers, List<String> inviterNumbers) {
        CombineDataModel combineDataModel = new CombineDataModel(new ArrayList<String>(), inviterNumbers);
        Log.d("manoj", "refer data : " + combineDataModel.getJsonObj().toString());
        progressBar.setVisibility(View.VISIBLE);
        Rest.connect().POST(combineDataModel.getJsonObj()).load(Constants.COMBINE_URL).as(Model.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                Log.d("manoj", "refer success");
                progressBar.setVisibility(View.GONE);
                finish();
            }

            @Override
            public void onRequestError(RequestError error) {
                Log.d("manoj", "refer failed, try again");
                showToastMsg(error.getErrorMessage());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public int getButtonResourceFile() {
        return R.drawable.icon_wine_glass;
    }
}
