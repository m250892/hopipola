package cleanslatetechnologies.com.hoppiapp.contactselection;

import com.example.manoj.utilshelper.model.Model;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 16/06/16.
 */
public class FriendsListResponseModel extends Model {

    @SerializedName("message")
    private List<String> friendsList;

    public List<String> getFriendsList() {
        return friendsList;
    }
}
