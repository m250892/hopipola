package cleanslatetechnologies.com.hoppiapp.contactselection.row;

import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;

/**
 * Created by manoj on 11/06/16.
 */
//ContactSelection List Data  Model
public class CSContactModel extends BaseCSListModel {
    private ContactDataModel contactData;
    private ContactType contactType;
    private CSListRowType rowType;

    public CSContactModel(ContactDataModel contactData, ContactType contactType) {
        this.contactData = contactData;
        this.contactType = contactType;
        this.rowType = CSListRowType.UNSELECT;
    }

    public void setContactData(ContactDataModel contactData) {
        this.contactData = contactData;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public void setRowType(CSListRowType rowType) {
        this.rowType = rowType;
    }

    public ContactDataModel getContactData() {
        return contactData;
    }

    public CSListRowType getRowType() {
        return rowType;
    }

    @Override
    public boolean keepObjct(String query) {
        if (contactData.getName() == null) {
            return false;
        }
        if (query == null) {
            return true;
        }
        return contactData.getName().toLowerCase().contains(query.toLowerCase());
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void toggleRowType() {
        rowType = rowType == CSListRowType.UNSELECT ? CSListRowType.SELECT : CSListRowType.UNSELECT;
    }
}
