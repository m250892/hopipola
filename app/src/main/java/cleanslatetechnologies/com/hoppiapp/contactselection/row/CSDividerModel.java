package cleanslatetechnologies.com.hoppiapp.contactselection.row;

/**
 * Created by manoj on 11/06/16.
 */
public class CSDividerModel extends BaseCSListModel {
    private String data;

    public CSDividerModel(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    public CSListRowType getRowType() {
        return CSListRowType.DIVIDER;
    }

    @Override
    public boolean keepObjct(String query) {
        return true;
    }
}
