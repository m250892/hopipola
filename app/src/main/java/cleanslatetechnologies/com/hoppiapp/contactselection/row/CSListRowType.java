package cleanslatetechnologies.com.hoppiapp.contactselection.row;

/**
 * Created by manoj on 10/06/16.
 */
public enum CSListRowType {
    NONE,
    SELECT,
    UNSELECT,
    DIVIDER;

    public static CSListRowType fromValue(int value) {
        for (CSListRowType rowType : CSListRowType.values()) {
            if (rowType.ordinal() == value) {
                return rowType;
            }
        }
        return NONE;
    }
}
