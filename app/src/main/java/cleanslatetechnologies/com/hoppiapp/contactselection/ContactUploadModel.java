package cleanslatetechnologies.com.hoppiapp.contactselection;

import com.example.manoj.utilshelper.core.GsonUtils;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 15/06/16.
 */
public class ContactUploadModel {
    @SerializedName("user_number")
    private String userNumber;
    @SerializedName("initial_info")
    private boolean isFirstTime;

    @SerializedName("contacts")
    private List<TmpContactDataModel> contacts;

    public ContactUploadModel(boolean isFirstTime, List<TmpContactDataModel> contacts) {
        userNumber = UserProfileManager.getUserDetail().getPhoneNumber();
        this.isFirstTime = isFirstTime;
        this.contacts = contacts;
    }

    public JSONObject getJsonObj() {
        JSONObject result = null;
        try {
            result = new JSONObject(GsonUtils.createJSONStringFromObject(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
