package cleanslatetechnologies.com.hoppiapp.contactselection;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.manoj.utilshelper.core.ArrayUtils;
import com.example.manoj.utilshelper.core.FileVersionHelper;
import com.example.manoj.utilshelper.core.GsonUtils;
import com.example.manoj.utilshelper.interfaces.IPermissionsCallback;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseSingleActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.FriendUnSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.FriendsSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.NewUserSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering.NewUserUnSelected;
import cleanslatetechnologies.com.hoppiapp.contactselection.model.ContactSaveModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.BaseCSListModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSContactModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSDividerModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.utils.ContactHelper;
import cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse.CombineRequestResponse;
import cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse.WaitingFriendModel;
import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

public class ContactCombineActivity extends BaseSingleActivity implements IPermissionsCallback, ContactFetcherTask.IContactFetchSuccessCallback, ContactRecyclerAdapter.IOnItemClickListener {

    private ContactFetcherTask contactFetcherTask;
    private List<BaseCSListModel> dataList;
    private ContactRecyclerAdapter adapter;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    private FileVersionHelper fileVersionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_selection);
        ButterKnife.bind(this);
        fileVersionHelper = new FileVersionHelper();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fabSelectContactRefer);
        floatingActionButton.setImageResource(getButtonResourceFile());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contact_selection_recyceler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        //setting adapter
        dataList = new ArrayList<>();
        adapter = new ContactRecyclerAdapter(this, dataList);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);

        //fetch contacts
        if (ArrayUtils.isCollectionFilled(UserProfileManager.getInstance().getUserContacts())) {
            onContactFetchSuccess();
        } else {
            ContactSaveModel data = null;
            if (fileVersionHelper.isFileExist(Constants.CONTACT_FILE_NAME)) {
                data = fileVersionHelper.getModel(Constants.CONTACT_FILE_NAME, ContactSaveModel.class);
            }
            if (data != null && data.getCount() > 0) {
                UserProfileManager.getInstance().updateContacts(data.getContacts());
                onContactFetchSuccess();
            } else {
                requestPermission(IPermissionsCallback.READ_CONTACTS, this);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.contact_activity_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setOnQueryTextListener(searchTextListener);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private SearchView.OnQueryTextListener searchTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            Log.d("manoj", "query text changed : " + newText);
            adapter.setFilter(newText);
            return false;
        }
    };

    @Override
    public void onPermissionRequestResult(int requestCode, boolean result) {
        if (requestCode == IPermissionsCallback.READ_CONTACTS && result) {
            Log.d("manoj", "get contact read permissition");
            if (contactFetcherTask == null) {
                progressBar.setVisibility(View.VISIBLE);
                contactFetcherTask = new ContactFetcherTask(this, this);
                contactFetcherTask.execute();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (contactFetcherTask != null) {
            contactFetcherTask.unregisterCallback();
        }
        super.onDestroy();
    }

    @Override
    public void onContactFetchSuccess() {
        ContactSaveModel data = null;
        if (!fileVersionHelper.isFileExist(Constants.CONTACT_FILE_NAME)) {
            //saving data
            ContactSaveModel contactSaveModel = new ContactSaveModel(UserProfileManager.getInstance().getUserContacts());
            fileVersionHelper.updateData(Constants.CONTACT_FILE_NAME, contactSaveModel);
        }

        progressBar.setVisibility(View.GONE);
        Log.d("manoj", "Contact fetched");
        contactFetcherTask = null;
        String url = Constants.GET_FRIENDS_API + UserProfileManager.getUserDetail().getPhoneNumber();
        progressBar.setVisibility(View.VISIBLE);
        Rest.connect()
                .GET()
                .load(url)
                .as(FriendsListResponseModel.class)
                .withCallback(new RequestCallback<FriendsListResponseModel>() {
                    @Override
                    public void onRequestSuccess(FriendsListResponseModel data) {
                        Log.d("manoj", "friend request success : ");
                        progressBar.setVisibility(View.GONE);
                        UserProfileManager.getInstance().setFirendsContacts(data.getFriendsList());
                        onFriendsListSuccess();
                    }

                    @Override
                    public void onRequestError(RequestError error) {
                        onContactFetchFailed();
                    }
                });


    }

    private void onFriendsListSuccess() {
        List<ContactDataModel> userContacts = UserProfileManager.getInstance().getUserContacts();
        Log.d("manoj", GsonUtils.createJSONStringFromObject(userContacts));
        //from api
        List<String> phoneNumbers = UserProfileManager.getInstance().getFirendsContacts();
        ContactHelper contactHelper = new ContactHelper();
        List<BaseCSListModel> tmpDataList = contactHelper.getCSListData(userContacts, phoneNumbers);


        startContactUpload(tmpDataList);
        updateDataInList(buildContactListRow(tmpDataList));
    }

    //Upload contacts to internal server
    private void startContactUpload(List<BaseCSListModel> tmpDataList) {

    }

    private void updateDataInList(List<BaseCSListModel> baseCSListModels) {
        this.dataList.clear();
        this.dataList.addAll(baseCSListModels);
        //build wraper list
        adapter.onOriginalListChange();
    }

    protected List<BaseCSListModel> buildContactListRow(List<BaseCSListModel> data) {
        List<BaseCSListModel> result = new ArrayList<>();

        List<BaseCSListModel> tmpList = new FriendsSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Combined with friends : "));
            result.addAll(tmpList);
            tmpList.clear();
        }

        tmpList = new NewUserSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Invite friends :"));
            result.addAll(tmpList);
            tmpList.clear();
        }

        tmpList = new FriendUnSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Select friends to combine with :"));
            result.addAll(tmpList);
            tmpList.clear();
        }

        tmpList = new NewUserUnSelected().getContacts(data);
        if (ArrayUtils.isCollectionFilled(tmpList)) {
            result.add(new CSDividerModel("Select friends to invite :"));
            result.addAll(tmpList);
            tmpList.clear();
        }
        return result;
    }


    @Override
    public void onContactFetchFailed() {
        contactFetcherTask = null;
        Toast.makeText(this, "Some error occure, Please try again", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onContactSelected(int position) {
        Log.d("manoj", "Item selected : " + position);
        BaseCSListModel selectedModel = adapter.getObj(position);
        if (selectedModel instanceof CSContactModel) {
            CSContactModel contactModel = (CSContactModel) selectedModel;
            contactModel.toggleRowType();
        }
        updateDataInList(buildContactListRow(dataList));
    }

    @OnClick(R.id.fabSelectContactRefer)
    public void onCombineBtnSelected() {
        List<BaseCSListModel> combineContats = new FriendsSelected().getContacts(dataList);
        List<BaseCSListModel> inviteContacts = new NewUserSelected().getContacts(dataList);

        List<String> combineNumbers = new ArrayList<>();
        for (BaseCSListModel data : combineContats) {
            if (data instanceof CSContactModel) {
                CSContactModel csContactModel = (CSContactModel) data;
                combineNumbers.add(csContactModel.getContactData().getPhoneNumber());
            }
        }

        List<String> inviterNumbers = new ArrayList<>();
        for (BaseCSListModel data : inviteContacts) {
            if (data instanceof CSContactModel) {
                CSContactModel csContactModel = (CSContactModel) data;
                inviterNumbers.add(csContactModel.getContactData().getPhoneNumber());
            }
        }

        sendCombineRequest(combineNumbers, inviterNumbers);
    }

    protected void sendCombineRequest(final List<String> combineNumbers, List<String> inviterNumbers) {
        CombineDataModel combineDataModel = new CombineDataModel(combineNumbers, inviterNumbers);
        Log.d("manoj", "combine data : " + combineDataModel.getJsonObj().toString());
        progressBar.setVisibility(View.VISIBLE);
        Rest.connect().POST(combineDataModel.getJsonObj()).load(Constants.COMBINE_URL).as(Model.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                Log.d("manoj", "combine success");
                progressBar.setVisibility(View.GONE);
                CombineRequestResponse combineRequestResponse = new CombineRequestResponse(getWaitingFriendList(combineNumbers));
                Intent intent = new Intent();
                intent.putExtra(Constants.PARAM_COMBINE_FRIENDS_DATA, combineRequestResponse);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onRequestError(RequestError error) {
                progressBar.setVisibility(View.GONE);
                Log.d("manoj", "combine failed, try again");
            }
        });
    }

    private List<WaitingFriendModel> getWaitingFriendList(List<String> combineNumbers) {
        List<WaitingFriendModel> result = new ArrayList<>();
        List<ContactDataModel> contacts = UserProfileManager.getInstance().getUserContacts();
        for (String number : combineNumbers) {
            String name = "";
            for (ContactDataModel contact : contacts) {
                if (contact.getPhoneNumber().equals(number)) {
                    name = contact.getName();
                    break;
                }
            }
            result.add(new WaitingFriendModel(name, number));
        }
        return result;
    }

    public void showToastMsg(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }


    public int getButtonResourceFile() {
        return R.drawable.icon_wine_glasses;
    }
}
