package cleanslatetechnologies.com.hoppiapp.contactselection.row;

/**
 * Created by manoj on 11/06/16.
 */
public enum ContactType {
    NONE,
    FRIEND,
    NEW_USER
}
