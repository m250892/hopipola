package cleanslatetechnologies.com.hoppiapp.contactselection;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Anubhaw on 5/17/2016.
 */
public class TmpContactDataModel {
    public String name;
    public Set<String> numbers;
    public Set<String> emailId;

    public TmpContactDataModel() {
        numbers = new LinkedHashSet<>();
        emailId = new LinkedHashSet<>();
    }

    public TmpContactDataModel(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getNumbers() {
        return numbers;
    }

    public void addNumber(String number) {
        numbers.add(number);
    }

    public List<String> getEmailId() {
        return new ArrayList<>(emailId);
    }

    public void addEmail(String email) {
        emailId.add(email);
    }

    public boolean isValid() {
        return getNumbers().size() > 0;
    }
}
