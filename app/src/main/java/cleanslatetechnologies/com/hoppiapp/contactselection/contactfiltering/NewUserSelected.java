package cleanslatetechnologies.com.hoppiapp.contactselection.contactfiltering;

import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSContactModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSListRowType;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.ContactType;

/**
 * Created by manoj on 11/06/16.
 */
public class NewUserSelected extends ContactCriteria {
    @Override
    protected boolean keepObject(CSContactModel tmpObj) {
        return tmpObj.getContactType() == ContactType.NEW_USER && tmpObj.getRowType() == CSListRowType.SELECT;
    }
}
