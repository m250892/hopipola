package cleanslatetechnologies.com.hoppiapp.contactselection.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cleanslatetechnologies.com.hoppiapp.contactselection.TmpContactDataModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.BaseCSListModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.CSContactModel;
import cleanslatetechnologies.com.hoppiapp.contactselection.row.ContactType;
import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;

/**
 * Created by manoj on 10/06/16.
 */
public class ContactHelper {

    public List<BaseCSListModel> getCSListData(List<ContactDataModel> contacts, List<String> phoneNumbers) {
        List<BaseCSListModel> result = new ArrayList<>();
        Set<String> phoneNumberSet = new HashSet<>();
        phoneNumberSet.addAll(phoneNumbers);
        for (ContactDataModel userContact : contacts) {
            ContactType contactType = phoneNumberSet.contains(userContact.getPhoneNumber()) ? ContactType.FRIEND : ContactType.NEW_USER;
            CSContactModel csContactModel = new CSContactModel(userContact, contactType);
            result.add(csContactModel);
        }
        return result;
    }

    public ArrayList<ContactDataModel> breakContactWithMultipleNumber(TmpContactDataModel contact) {
        ArrayList<ContactDataModel> result = new ArrayList<>();
        for (String number : contact.getNumbers()) {
            result.add(new ContactDataModel(contact.getName(), number, contact.getEmailId()));
        }
        return result;
    }

    //get valid number
    public String validateNumber(String num) {
        String result = null;
        num = num.replaceAll("\\D+", "");
        int len = num.length();
        if (len < 10) {
            result = null;
        } else if (len >= 10) {
            result = num.substring(len - 10);
        }
        return result;
    }

}
