package cleanslatetechnologies.com.hoppiapp.contactselection.model;

import com.example.manoj.utilshelper.core.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;

/**
 * Created by manoj on 21/06/16.
 */
public class ContactSaveModel {
    private int count;
    private List<ContactDataModel> contacts;

    public ContactSaveModel(List<ContactDataModel> contacts) {
        if (!ArrayUtils.isCollectionFilled(contacts)) {
            contacts = new ArrayList<>();
        }
        count = contacts.size();
        this.contacts = contacts;
    }

    public int getCount() {
        return count;
    }

    public List<ContactDataModel> getContacts() {
        return contacts;
    }
}
