package cleanslatetechnologies.com.hoppiapp.contactselection.row;

/**
 * Created by manoj on 11/06/16.
 */
public abstract class BaseCSListModel {
    public abstract CSListRowType getRowType();

    public abstract boolean keepObjct(String query);
}
