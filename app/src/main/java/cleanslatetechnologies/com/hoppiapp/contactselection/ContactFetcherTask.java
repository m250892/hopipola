package cleanslatetechnologies.com.hoppiapp.contactselection;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.contactselection.utils.ContactHelper;
import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 10/06/16.
 */
public class ContactFetcherTask extends AsyncTask<Void, Void, List<ContactDataModel>> {

    private ContactHelper contactHelper;
    private Context context;
    private List<TmpContactDataModel> tmpContactDataModels = new ArrayList<>();

    private IContactFetchSuccessCallback callback;

    public ContactFetcherTask(IContactFetchSuccessCallback callback, Context context) {
        this.callback = callback;
        this.context = context;
        contactHelper = new ContactHelper();
    }


    @Override
    protected List<ContactDataModel> doInBackground(Void... params) {
        List<ContactDataModel> data = fetchContacts();
        return data;
    }

    @Override
    protected void onPostExecute(final List<ContactDataModel> contactDataModels) {
        if (!UserProfileManager.getInstance().isContactUploaded()) {
            ContactUploadModel uploadModel = new ContactUploadModel(true, tmpContactDataModels);
            Log.d("manoj", uploadModel.getJsonObj().toString());
            Rest.connect().POST(uploadModel.getJsonObj()).load(Constants.CONTACT_UPLOAD_URL).as(Model.class).withCallback(new RequestCallback() {
                @Override
                public void onRequestSuccess(Object o) {
                    Log.d("manoj", "Contact upload success");
                    UserProfileManager.getInstance().setContactUploaded(true);
                    UserProfileManager.getInstance().updateContacts(contactDataModels);
                    if (callback != null) {
                        callback.onContactFetchSuccess();
                    }
                }

                @Override
                public void onRequestError(RequestError error) {
                    Log.d("manoj", "Contact upload failed");
                    if (callback != null) {
                        callback.onContactFetchFailed();
                    }
                }
            });
        } else {
            UserProfileManager.getInstance().updateContacts(contactDataModels);
            if (callback != null) {
                callback.onContactFetchSuccess();
            }
        }
    }

    private List<ContactDataModel> fetchContacts() {
        //initialize result
        List<ContactDataModel> contactDataModels = new ArrayList<>();

        //contact main table
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        //phone table
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        //email table
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        int maxCount = Integer.MAX_VALUE;
        while (cursor.moveToNext() && maxCount > 0) {

            String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
            String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
            TmpContactDataModel contact = new TmpContactDataModel(name);
            int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

            //isPhonenumber exist
            if (hasPhoneNumber > 0) {
                maxCount--;
                //This is to read multiple phone numbers associated with the same contact
                Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                while (phoneCursor.moveToNext()) {
                    String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                    phoneNumber = contactHelper.validateNumber(phoneNumber);
                    if (phoneNumber != null) {
                        contact.addNumber(phoneNumber);
                    }
                }
                phoneCursor.close();

                // Read every email id associated with the contact
                Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
                while (emailCursor.moveToNext()) {
                    String email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                    if (!TextUtils.isEmpty(email)) {
                        contact.addEmail(email);
                    }
                }
                emailCursor.close();
            }

            if (contact.isValid()) {
                tmpContactDataModels.add(contact);
                contactDataModels.addAll(contactHelper.breakContactWithMultipleNumber(contact));
            }
        }

        return contactDataModels;
    }


    void unregisterCallback() {
        callback = null;
    }

    interface IContactFetchSuccessCallback {
        void onContactFetchSuccess();

        void onContactFetchFailed();
    }

}
