package cleanslatetechnologies.com.hoppiapp.tags;

public interface TagAdapterCallback {
    void onTagClicked(int position);
}