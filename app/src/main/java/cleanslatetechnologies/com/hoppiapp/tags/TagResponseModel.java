package cleanslatetechnologies.com.hoppiapp.tags;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 17/06/16.
 */
public class TagResponseModel {
    @SerializedName("message")
    private List<String> tags;

    public List<String> getTags() {
        return tags;
    }
}
