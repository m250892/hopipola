package cleanslatetechnologies.com.hoppiapp.tags;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;

/**
 * Created by manoj on 17/06/16.
 */
public class UserTagAdapter extends RecyclerView.Adapter<UserTagAdapter.ViewHolder> {

    private Context context;
    private List<String> tags;
    private TagAdapterCallback callback;

    public UserTagAdapter(Context context, List<String> tags, TagAdapterCallback callback) {
        this.context = context;
        this.tags = tags;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View baseView = LayoutInflater.from(context).inflate(R.layout.layout_user_selected_tag_view, null);
        return new ViewHolder(baseView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(tags.get(position));
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tag_text_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onTagClicked(getAdapterPosition());
        }
    }
}
