package cleanslatetechnologies.com.hoppiapp.tags;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;

public class SearchSuggestionAdapter extends BaseAdapter implements Filterable {
    private static final String TAG = "SearchSuggestion";
    private Context context;
    private List<String> recentSearches;

    public SearchSuggestionAdapter(Context context, List<String> recentSearched) {
        this.context = context;
        this.recentSearches = recentSearched;
    }

    @Override
    public int getCount() {
        return recentSearches.size();
    }

    @Override
    public String getItem(int position) {
        return recentSearches.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.tag_suggestion_item, parent, false);
        }
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(getItem(position));
        return convertView;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.d("manoj", "perform filtering" + constraint);
                FilterResults result = new FilterResults();
                result.values = recentSearches;
                result.count = getCount();
                return result;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            }
        };
        return filter;
    }

    /*//display new filter results
    private void updateResult(List<String> values) {
        Log.d("mano", "Update result from filter : " + values);
        recentSearches.clear();
        if (values != null) {
            recentSearches.addAll(values);
        }
        notifyDataSetChanged();
    }*/

}
