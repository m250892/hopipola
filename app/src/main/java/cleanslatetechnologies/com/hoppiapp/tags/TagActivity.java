package cleanslatetechnologies.com.hoppiapp.tags;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.manoj.utilshelper.core.GsonUtils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.model.Model;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.homescreen.HomeActivity;
import cleanslatetechnologies.com.hoppiapp.homescreen.HomeDataStore;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

public class TagActivity extends AppCompatActivity implements TagAdapterCallback {

    private static final String ADD_NEW_TAG_TEXT = "Add";
    @Bind(R.id.tag_search_view)
    AutoCompleteTextView searchTagView;
    private SearchSuggestionAdapter searchAdapter;
    private List<String> suggestedTags = new ArrayList<>();
    private String lastQueryRequest;

    private RecyclerView selectedTagRecyclerView;
    private List<String> userTags = new ArrayList<>();
    private UserTagAdapter userTagAdapter;

    private static final int PARAM_CACHE_SIZE = 5;
    private LruCache<String, List<String>> localCache;

    private String lastBeforeSearchQuery;

    public TagActivity() {
        localCache = new LruCache<>(PARAM_CACHE_SIZE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);
        ButterKnife.bind(this);
        setViews();
    }

    private void setViews() {

        //user tag adapter
        selectedTagRecyclerView = (RecyclerView) findViewById(R.id.user_selected_tags);
        userTagAdapter = new UserTagAdapter(this, userTags, this);
        selectedTagRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        selectedTagRecyclerView.setAdapter(userTagAdapter);

        searchAdapter = new SearchSuggestionAdapter(this, suggestedTags);
        searchTagView.setAdapter(searchAdapter);
        searchTagView.setThreshold(0);

        searchTagView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("manoj", "Item click position : " + position);
                //if last item for add new tag
                if (position == suggestedTags.size() - 1) {
                    addNewTag(lastBeforeSearchQuery);
                } else {
                    addNewTag(suggestedTags.get(position));
                }
                lastQueryRequest = "";
                searchTagView.setText(lastQueryRequest);
            }
        });
        searchTagView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                lastBeforeSearchQuery = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                onSearchQuerySubmit(s.toString());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchTagView.postDelayed(new Runnable() {
            @Override
            public void run() {
                showSoftKeyBoard(searchTagView);
            }
        }, 100);
    }

    protected void showSoftKeyBoard(EditText et) {
        if (null != et && this != null) {
            et.requestFocus();
            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void addNewTag(String s) {
        Log.d("manoj", "add new tag : " + s);
        userTags.add(s);
        userTagAdapter.notifyDataSetChanged();
        hideKeyboard();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void onSearchQuerySubmit(String searchQuery) {
        if (searchQuery.equals(lastQueryRequest)) {
            return;
        }
        fetchResult(searchQuery);
    }

    private void fetchResult(final String searchQuery) {
        lastQueryRequest = searchQuery;
        List<String> cacheData = localCache.get(searchQuery);
        if (cacheData != null) {
            onSearchComplete(cacheData);
            return;
        }

        //Fetch new suggestoin
        Rest.connect().GET().load(getSearchUrl(searchQuery)).as(TagResponseModel.class).withCallback(new RequestCallback<TagResponseModel>() {
            @Override
            public void onRequestSuccess(TagResponseModel data) {
                localCache.put(searchQuery, data.getTags());
                if (searchQuery.equals(getCurrentQuery())) {
                    onSearchComplete(data.getTags());
                }
            }

            @Override
            public void onRequestError(RequestError error) {
                Log.d("mano", "error on getting tag");
            }
        });
    }

    private String getSearchUrl(String searchQuery) {
        return Constants.SEARCH_TAG_URL + searchQuery;
    }

    private void onSearchComplete(List<String> data) {
        Log.d("manoj", "onSearch tag result new data " + data);
        suggestedTags.clear();
        suggestedTags.addAll(data);
        //dummy last tag
        suggestedTags.add(ADD_NEW_TAG_TEXT + " " + getCurrentQuery());
        searchAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.reminde_me_later)
    public void onRemindMeLaterBtnClick() {
        finish();
    }

    @OnClick(R.id.done_tags)
    public void onDoneBtnClick() {

        TagUpdateModel tagUpdateModel = new TagUpdateModel(userTags);
        Rest.connect().POST(tagUpdateModel.getJsonObj()).load(Constants.UPDAE_TAGS).as(Model.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                onTagsUpdateDone();
            }

            @Override
            public void onRequestError(RequestError error) {
                showToast(error.getErrorMessage());
            }
        });


    }

    private void showToast(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void onTagsUpdateDone() {
        HomeDataStore.getInstance().updateLastTags(userTags);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public String getCurrentQuery() {
        return searchTagView.getText().toString();
    }

    @Override
    public void onTagClicked(int position) {
        userTags.remove(position);
        userTagAdapter.notifyDataSetChanged();
    }

    class TagUpdateModel {
        private List<String> tags;
        @SerializedName("creator")
        private String userNumber;

        public TagUpdateModel(List<String> tags) {
            this.tags = tags;
            userNumber = UserProfileManager.getUserDetail().getPhoneNumber();
        }

        public JSONObject getJsonObj() {
            JSONObject result = null;
            try {
                result = new JSONObject(GsonUtils.createJSONStringFromObject(this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
