package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;

/**
 * Created by manoj on 09/06/16.
 */
public class RewardsAdapter extends RecyclerView.Adapter<RewardsAdapter.ViewHolder> {

    private Context context;
    private List<RewardDataModel> dataList;
    private RecycelItemClicked callback;

    public RewardsAdapter(Context context, List<RewardDataModel> dataList, RecycelItemClicked callback) {
        this.context = context;
        this.dataList = dataList;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View baseView = LayoutInflater.from(context).inflate(R.layout.layout_my_rewards_row, null);
        return new ViewHolder(baseView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("manoj", "rewards bind view holder " + position);
        RewardDataModel data = dataList.get(position);
        holder.textView.setText(data.getNames());
        holder.iconView.setImageDrawable(ContextCompat.getDrawable(context, data.getCategory().getDrawableId()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ImageView iconView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.item_name);
            iconView = (ImageView) itemView.findViewById(R.id.dish_type_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (callback != null) {
                callback.onItemSelected(getAdapterPosition());
            }
        }
    }
}

