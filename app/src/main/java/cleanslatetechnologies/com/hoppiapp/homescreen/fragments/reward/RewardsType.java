package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import cleanslatetechnologies.com.hoppiapp.R;

/**
 * Created by manoj on 15/06/16.
 */
public enum RewardsType {
    Premium_DRINK("Premium Hard Drink", R.drawable.icon_beer),
    MAIN_DRINK("Main Hard Drink", R.drawable.icon_beer),
    MOCKTAIL("Mocktail", R.drawable.icon_beer),
    COCKTAIL("Cocktail", R.drawable.icon_wine),
    SHOT("Shot", R.drawable.icon_shots),
    BREEZER("Breezer", R.drawable.icon_beer),
    STARTER("Starter", R.drawable.icon_french_fries),
    BEER("Beer", R.drawable.icon_beer),
    DESSERT("Dessert", R.drawable.icon_desset),
    NONE("None", R.drawable.icon_beer);


    private String type;
    private int drawableId;

    RewardsType(String type, int drawableId) {
        this.type = type;
        this.drawableId = drawableId;
    }

    public String getType() {
        return type;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public static RewardsType getRewarType(String type) {
        for (RewardsType rewardsType : RewardsType.values()) {
            if (rewardsType.getType() == type) {
                return rewardsType;
            }
        }
        return NONE;
    }
}
