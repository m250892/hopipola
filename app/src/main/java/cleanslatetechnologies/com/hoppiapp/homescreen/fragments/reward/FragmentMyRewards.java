package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import android.net.Uri;
import android.view.View;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 15/06/16.
 */
public class FragmentMyRewards extends FragmentBaseRewards {


    private View retryView;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_my_rewards_card;
    }


    @Override
    protected void initViews(View baseView) {
        super.initViews(baseView);
        retryView = baseView.findViewById(R.id.retry_view);
        retryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchMyRewards();
            }
        });
    }

    @Override
    protected void onMyRewardsChange(List<RewardDataModel> myRewards) {
        super.onMyRewardsChange(myRewards);
        retryView.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMyRewards();
    }

    @Override
    protected String getUrl() {
        Uri.Builder uri = Uri.parse(Constants.MY_REWARDS).buildUpon();
        String userData = UserProfileManager.getUserDetail().getPhoneNumber();
        uri.appendQueryParameter("user", userData);
        return uri.build().toString();
    }
}

