package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import java.util.List;

/**
 * Created by manoj on 18/06/16.
 */
public class ShareRewardEvent {
    private List<String> numbers;

    public ShareRewardEvent(List<String> numbers) {
        this.numbers = numbers;
    }

    public List<String> getNumbers() {
        return numbers;
    }
}
