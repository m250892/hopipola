package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.model;

/**
 * Created by manoj on 18/06/16.
 */
public class HomeTagModel extends BaseTagModel {
    private String rating;

    public HomeTagModel(String text, String rating) {
        super(text);
        this.rating = rating;
    }

    @Override
    public TagType getTagType() {
        return TagType.TAG;
    }
}
