package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import com.example.manoj.utilshelper.model.Model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * Created by manoj on 15/06/16.
 */
public class RewardDataModel extends Model implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private String price;
    @SerializedName("category")
    private String category;

    public String getNames() {
        String result = null;
        if (name == null) {
            result = "";
            return result;
        }
        StringTokenizer st = new StringTokenizer(name, "+");
        while (st.hasMoreTokens()) {
            if (result == null) {
                result = st.nextToken();
            } else {
                result += "\n+\n";
                result += st.nextToken();
            }
        }
        return result;
    }

    public String getPrice() {
        return price;
    }

    public RewardsType getCategory() {
        return RewardsType.getRewarType(category);
    }
}
