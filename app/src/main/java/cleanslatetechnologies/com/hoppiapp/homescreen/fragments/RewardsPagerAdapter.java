package cleanslatetechnologies.com.hoppiapp.homescreen.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward.FragmentMyRewards;
import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward.FragmentShareRewards;

public class RewardsPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private List<String> titles;

    public RewardsPagerAdapter(FragmentManager fm) {
        super(fm);
        titles = new ArrayList<>();
        titles.add("My Rewards");
        titles.add("Shared Rewards");
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentMyRewards();
        } else {
            return new FragmentShareRewards();
        }
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }


}
