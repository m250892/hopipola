package cleanslatetechnologies.com.hoppiapp.homescreen.fragments;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.feedback.FeedbackActivity;
import cleanslatetechnologies.com.hoppiapp.models.TagModel;
import cleanslatetechnologies.com.hoppiapp.tags.TagActivity;

/**
 * Created by manoj on 09/06/16.
 */
public class HomeFragment extends BaseHomeFragment {

    private TextView addressTextView;
    private TextView timingTextView;
    private TextView distanceTextView;
    private TextView timeToReachTextView;
    private TextView pointTextView;
    private TextView ratingTextView;

    private ArrayList<TextView> tags;

    public static Fragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        updateUserTags();
    }

    private void updateUserTags() {
        List<TagModel> tagData = homeData.getUserTags();
        for (int i = 0; i < tagData.size() && i < tags.size(); i++) {
            tags.get(i).setText(tagData.get(i).getName() + " \u2665" + tagData.get(i).getScore());
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViews(View baseView) {
        ratingTextView = (TextView) baseView.findViewById(R.id.rating_percent);
        addressTextView = (TextView) baseView.findViewById(R.id.address_view);
        timingTextView = (TextView) baseView.findViewById(R.id.opening_timeing_view);
        distanceTextView = (TextView) baseView.findViewById(R.id.distance_view);
        timeToReachTextView = (TextView) baseView.findViewById(R.id.time_to_reach_view);
        pointTextView = (TextView) baseView.findViewById(R.id.points_view);

        //update progress
        ProgressBar progressBar = (ProgressBar) baseView.findViewById(R.id.rating_progress_bar);
        int progress = 0;
        try {
            progress = Integer.parseInt(homeData.getRating());
        } catch (Exception e) {
            e.printStackTrace();
        }
        progressBar.setProgress(progress);

        baseView.findViewById(R.id.ic_location_view).setOnClickListener(this);
        baseView.findViewById(R.id.rating_container).setOnClickListener(this);


        tags = new ArrayList<>();
        tags.add((TextView) baseView.findViewById(R.id.tvTag1));
        tags.add((TextView) baseView.findViewById(R.id.tvTag2));
        tags.add((TextView) baseView.findViewById(R.id.tvTag3));
        tags.add((TextView) baseView.findViewById(R.id.tvTag4));
        tags.add((TextView) baseView.findViewById(R.id.tvTag5));

        updateViews();
    }

    private void updateViews() {
        addressTextView.setText(homeData.getAddress());
        timingTextView.setText(homeData.getTimeingText());
        distanceTextView.setText(homeData.getDistance());
        timeToReachTextView.setText(homeData.getTime());
        pointTextView.setText(homeData.getUserPoints());
        ratingTextView.setText(homeData.getRating() + "%");
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ic_location_view) {
            openGoogleMapApplication();
        } else if (v.getId() == R.id.rating_container) {
            Intent intent = new Intent(getContext(), FeedbackActivity.class);
            startActivity(intent);
        }
        super.onClick(v);
    }

    public String getLatLongString() {
        return homeData.getLocationLat() + "," + homeData.getLocationLong();
    }

    private void openGoogleMapApplication() {
        Uri gmmIntentUri = Uri.parse("geo:0,0?z=12&q=" + getLatLongString());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }


    @OnClick(R.id.add_new_tag_btn)
    public void onAddTagClicked() {
        Intent intent = new Intent(getContext(), TagActivity.class);
        startActivityForResult(intent, Constants.PARAM_ADD_NEW_TAG_REQUEST_CODE);
    }
}
