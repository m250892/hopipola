package cleanslatetechnologies.com.hoppiapp.homescreen.fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 09/06/16.
 */
public class MyJournyFragment extends BaseHomeFragment {
    private ViewPager pager;
    private RewardsPagerAdapter rewardsAdapter;

    public static Fragment newInstance() {
        MyJournyFragment myJournyFragment = new MyJournyFragment();
        return myJournyFragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_my_journy;
    }

    @Override
    protected void initViews(View baseView) {
        pager = (ViewPager) baseView.findViewById(R.id.layout_pager);
        rewardsAdapter = new RewardsPagerAdapter(getChildFragmentManager());
        pager.setAdapter(rewardsAdapter);
        TabLayout tabLayout = (TabLayout) baseView.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pager);

        TextView userName = (TextView) baseView.findViewById(R.id.user_name);
        userName.setText(UserProfileManager.getUserDetail().getName());
    }

}
