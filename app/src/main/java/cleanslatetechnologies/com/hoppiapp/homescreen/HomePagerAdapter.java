package cleanslatetechnologies.com.hoppiapp.homescreen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.HomeFragment;
import cleanslatetechnologies.com.hoppiapp.homescreen.fragments.MyJournyFragment;

/**
 * Created by manoj on 09/06/16.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private List<String> titles;

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
        titles = new ArrayList<>();
        titles.add("HOME");
        titles.add("MY REWARDS");
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return HomeFragment.newInstance();
        } else {
            return MyJournyFragment.newInstance();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public int getCount() {
        return titles.size();
    }
}