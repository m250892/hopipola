package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.utilshelper.managers.EventManager;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.ContactCombineActivity;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 15/06/16.
 */
public class FragmentShareRewards extends FragmentBaseRewards {
    private View noRewardView, contentView;
    private List<String> userFriendsNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventManager.getDefaultEventBus().register(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_share_rewards_card;
    }

    @Override
    protected void initViews(View baseView) {
        super.initViews(baseView);
        contentView = baseView.findViewById(R.id.content_view);
        noRewardView = baseView.findViewById(R.id.no_reward_view);
        baseView.findViewById(R.id.fabEmptyCombine).setOnClickListener(this);
    }

    @Subscribe
    public void onEvent(ShareRewardEvent data) {
        noRewardView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        userFriendsNumber = data.getNumbers();
        fetchMyRewards();
    }

    @Override
    public void onDestroyView() {
        EventManager.getDefaultEventBus().unregister(this);
        super.onDestroyView();
    }

    @Override
    protected String getUrl() {
        Uri.Builder uri = Uri.parse(Constants.MY_REWARDS).buildUpon();
        String userData = UserProfileManager.getUserDetail().getPhoneNumber();
        for (String friendNumber : userFriendsNumber) {
            userData += "," + friendNumber;
        }
        uri.appendQueryParameter("user", userData);
        return uri.build().toString();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fabEmptyCombine) {
            Intent intent = new Intent(getContext(), ContactCombineActivity.class);
            getActivity().startActivityForResult(intent, Constants.PARAM_COMBINE_RESULT_CODE);
        }
        super.onClick(v);
    }
}
