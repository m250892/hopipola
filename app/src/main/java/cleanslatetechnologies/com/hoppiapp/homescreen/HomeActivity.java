package cleanslatetechnologies.com.hoppiapp.homescreen;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.manoj.utilshelper.ui.BaseSingleActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.contactselection.ContactCombineActivity;
import cleanslatetechnologies.com.hoppiapp.contactselection.ReferFriendsActivity;
import cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse.CombineRequestResponse;
import cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse.WaitingForFriendDailog;
import cleanslatetechnologies.com.hoppiapp.feedback.FeedbackActivity;
import cleanslatetechnologies.com.hoppiapp.models.HomePageDataModel;
import cleanslatetechnologies.com.hoppiapp.setting.AboutUsActivity;
import cleanslatetechnologies.com.hoppiapp.setting.FaqsActivity;
import cleanslatetechnologies.com.hoppiapp.setting.SettingActivity;
import io.fabric.sdk.android.Fabric;

public class HomeActivity extends BaseSingleActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "cCQLy8iqsfJO9dFHmXYqss08F";
    private static final String TWITTER_SECRET = "G2jpag6dXxNBqTYWN2GfpsXXqbIznNeqe6GeYkC2XlxgwQLTsU";

    protected ViewPager viewPager;
    protected TabLayout tabLayout;
    private HomePagerAdapter pagerAdapter;
    private DrawerLayout drawer;
    private HomePageDataModel homeData;

    boolean isAnyViewSelected = false;

    @Bind(R.id.footer_combine_container)
    View footerCombineContainer;

    @Bind(R.id.footer_refer_container)
    View footerReferContainer;

    @Bind(R.id.footer_share_container)
    View footerShareContainer;

    @Bind(R.id.fabCombine)
    FloatingActionButton fabCombine;

    @Bind(R.id.fabRefer)
    FloatingActionButton fabRefer;

    @Bind(R.id.fabShare)
    FloatingActionButton fabShare;

    @Bind(R.id.header_combine_container)
    View headerCombineContainer;

    @Bind(R.id.header_refer_container)
    View headerReferContainer;

    private TwitterLoginButton twitterLoginButton;
    private LoginButton fbLoginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        //Geting home data
        homeData = HomeDataStore.getInstance().getHomeData();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        intigrateToolbarWithDrawer(toolbar);

        viewPager = (ViewPager) findViewById(R.id.tabanim_viewpager);
        pagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        getSupportActionBar().setTitle(getString(R.string.app_name));
        tabLayout = (TabLayout) findViewById(R.id.tabanim_tabs);
        tabLayout.setupWithViewPager(viewPager);

        ((NavigationView) findViewById(R.id.nvHome)).setNavigationItemSelectedListener(navListener);
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("manoj", "onActivity result in activity : " + requestCode + ", " + resultCode);

        if (resultCode == RESULT_OK) {
            //check if come from combine request
            if (requestCode == Constants.PARAM_COMBINE_RESULT_CODE) {
                CombineRequestResponse combineRequestResponse = (CombineRequestResponse) data.getExtras().getSerializable(Constants.PARAM_COMBINE_FRIENDS_DATA);
                WaitingForFriendDailog dialog = WaitingForFriendDailog.newInstance(combineRequestResponse);
                dialog.setCancelable(false);
                dialog.show(getSupportFragmentManager(), WaitingForFriendDailog.class.getCanonicalName());
            } else {
                twitterLoginButton.onActivityResult(requestCode, resultCode, data);
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    private NavigationView.OnNavigationItemSelectedListener navListener = new NavigationView.OnNavigationItemSelectedListener() {

        // This method will trigger on item Click of navigation menu
        @Override
        public boolean onNavigationItemSelected(MenuItem menuItem) {
            drawer.closeDrawer(GravityCompat.START);
            switch (menuItem.getItemId()) {
                case R.id.action_settings:
                    Intent setting = new Intent(getBaseContext(), SettingActivity.class);
                    startActivity(setting);
                    break;
                case R.id.action_faqs:
                    Intent faqs = new Intent(getBaseContext(), FaqsActivity.class);
                    startActivity(faqs);
                    break;
                case R.id.action_feedback:
                    Intent intent = new Intent(getBaseContext(), FeedbackActivity.class);
                    startActivity(intent);
                    break;
                case R.id.action_about:
                    Intent aboutUs = new Intent(getBaseContext(), AboutUsActivity.class);
                    startActivity(aboutUs);
                    break;
            }
            return true;
        }
    };

    public void intigrateToolbarWithDrawer(Toolbar toolbar) {
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
        };
        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                toggle.onDrawerSlide(drawerView, slideOffset);
            }

            @Override

            public void onDrawerOpened(View drawerView) {
                toggle.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggle.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                toggle.onDrawerStateChanged(newState);
            }
        });
        toggle.syncState();
    }


    @OnClick(R.id.fabCombine)
    public void onFabCombineClick() {
        Log.d("manoj", "fab combine click");
        if (footerCombineContainer.getVisibility() != View.VISIBLE) {
            //unselect other view
            if (isAnyViewSelected) {
                resetViewsColor();
                footerReferContainer.setVisibility(View.GONE);
                footerShareContainer.setVisibility(View.GONE);
                headerReferContainer.setVisibility(View.GONE);
            }
            headerCombineContainer.setVisibility(View.VISIBLE);
            footerCombineContainer.setVisibility(View.VISIBLE);
            isAnyViewSelected = true;
            fabCombine.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
            fabCombine.setImageDrawable(tintDrawable(R.drawable.icon_wine_glasses, R.color.white));
        } else {
            isAnyViewSelected = false;
            footerCombineContainer.setVisibility(View.GONE);
            headerCombineContainer.setVisibility(View.GONE);
            resetViewsColor();
        }
    }

    @OnClick(R.id.fabRefer)
    public void onFabReferClick() {
        if (footerReferContainer.getVisibility() != View.VISIBLE) {
            //unselect other view
            if (isAnyViewSelected) {
                resetViewsColor();
                footerCombineContainer.setVisibility(View.GONE);
                footerShareContainer.setVisibility(View.GONE);
                headerCombineContainer.setVisibility(View.GONE);
            }
            footerReferContainer.setVisibility(View.VISIBLE);
            headerReferContainer.setVisibility(View.VISIBLE);
            isAnyViewSelected = true;
            fabRefer.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
            fabRefer.setImageDrawable(tintDrawable(R.drawable.icon_wine_glass, R.color.white));
        } else {
            isAnyViewSelected = false;
            footerReferContainer.setVisibility(View.GONE);
            headerReferContainer.setVisibility(View.GONE);
            resetViewsColor();
        }
    }

    @OnClick(R.id.fabShare)
    public void onFabShareClick() {
        if (footerShareContainer.getVisibility() != View.VISIBLE) {
            //unselect other view
            if (isAnyViewSelected) {
                resetViewsColor();
                footerCombineContainer.setVisibility(View.GONE);
                footerReferContainer.setVisibility(View.GONE);
                headerReferContainer.setVisibility(View.GONE);
                headerCombineContainer.setVisibility(View.GONE);
            }
            footerShareContainer.setVisibility(View.VISIBLE);
            isAnyViewSelected = true;
            fabShare.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
            fabShare.setImageDrawable(tintDrawable(R.drawable.icon_social, R.color.white));
        } else {
            isAnyViewSelected = false;
            footerShareContainer.setVisibility(View.GONE);
            resetViewsColor();
        }
    }

    private void resetViewsColor() {
        ColorStateList colorWhite = getResources().getColorStateList(R.color.grey);
        fabCombine.setBackgroundTintList(colorWhite);
        fabRefer.setBackgroundTintList(colorWhite);
        fabShare.setBackgroundTintList(colorWhite);
        fabCombine.setImageDrawable(tintDrawable(R.drawable.icon_wine_glasses, R.color.colorPrimary));
        fabRefer.setImageDrawable(tintDrawable(R.drawable.icon_wine_glass, R.color.colorPrimary));
        fabShare.setImageDrawable(tintDrawable(R.drawable.icon_social, R.color.colorPrimary));
    }


    private Drawable tintDrawable(int drawableId, int colorId) {
        Drawable drawable = getResources().getDrawable(drawableId);
        int primaryColor = getResources().getColor(colorId);
        drawable.setColorFilter(primaryColor, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public boolean isFbLoggedIn() {
        return !(!fbLoginButton.isInEditMode() && AccessToken.getCurrentAccessToken() != null);
    }


    private void showToastMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    protected void initViews() {
        TextView unlockDishView = (TextView) findViewById(R.id.unlock_dish_count);
        unlockDishView.setText(homeData.getUnlockDishCount());

        TextView lockDishView = (TextView) findViewById(R.id.people_refered);
        lockDishView.setText(homeData.getLockDishCount());

        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                /*String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
                Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();*/
                TweetComposer.Builder builder = new TweetComposer.Builder(
                        getBaseContext()).text("#SpiritUp");
                builder.show();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });


        fbLoginButton = (LoginButton) findViewById(R.id.fb_login_btn);
        fbLoginButton.setReadPermissions("user_friends");
        // Callback registration
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("manoj", "fb login success");
                shareOnFb();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("manoj", "fb login cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("manoj", "fb login cancel" + exception);
            }
        });
    }

    private void shareOnFb() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();
        ShareDialog shareDialog = new ShareDialog(this);
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
    }

    @OnClick(R.id.fabEmptyCombine)
    public void handleCombineBtnClick() {
        Intent intent = new Intent(this, ContactCombineActivity.class);
        startActivityForResult(intent, Constants.PARAM_COMBINE_RESULT_CODE);
    }

    @OnClick(R.id.footer_combine_reward_btn)
    public void handleFooterCombineBtnClick() {
        handleCombineBtnClick();
    }

    @OnClick(R.id.medium_sms)
    public void handleInviteViaSms() {
        Intent contactSelection = new Intent(this, ReferFriendsActivity.class);
        startActivity(contactSelection);
    }

    @OnClick(R.id.medium_whatsapp)
    public void handleInviteViaWhatsapp() {
        if (appInstalled("com.whatsapp")) {
            Intent iWhatsapp = new Intent();
            iWhatsapp.setAction(Intent.ACTION_SEND);
            iWhatsapp.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            iWhatsapp.setType("text/plain");
            iWhatsapp.setPackage("com.whatsapp");
            startActivity(iWhatsapp);
        } else {
            showToastMessage("Whatsapp not installed on your phone.");
        }
    }

    @OnClick(R.id.medium_messanger)
    public void handleInviteViaMessanger() {
        if (appInstalled("com.facebook.orca")) {
            Intent iMessenger = new Intent();
            iMessenger.setAction(Intent.ACTION_SEND);
            iMessenger.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            iMessenger.setType("text/plain");
            iMessenger.setPackage("com.facebook.orca");
            startActivity(iMessenger);
        } else {
            showToastMessage("Messenger not installed on your phone.");
        }
    }


    @OnClick(R.id.share_instagram)
    public void shareInstagram() {
        Uri uri = Uri.parse("http://instagram.com");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            showToastMessage("Instagram app not installed");
        }
    }

    @OnClick(R.id.share_zomato)
    public void shareZomato(ImageButton imageButton) {
        if (imageButton.isSelected()) {
            //unselected
        } else {
            Log.d("manoj", "zomato btn click");
            String url = "https://www.zomato.com/mumbai/hoppipola-lower-parel";
            Uri uri = Uri.parse(url);
            Intent intent = new Intent();
            intent.setData(uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }


    @OnClick(R.id.share_fb)
    public void shareFb(ImageButton imageButton) {
        if (imageButton.isSelected()) {
            //do nothing
        } else {
            if (isFbLoggedIn()) {
                fbLoginButton.performClick();
            } else {
                //share link via fb
                shareOnFb();
            }
        }
    }


    @OnClick(R.id.share_twitter)
    public void shareTwitter(ImageButton imageButton) {
        if (imageButton.isSelected()) {
            Twitter.logOut();
        } else {
            PackageManager pkManager = this.getPackageManager();
            try {
                PackageInfo pkgInfo = pkManager.getPackageInfo("com.twitter.android", 0);
                twitterLoginButton.performClick();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                showToastMessage("Twitter app not installed");
            }
        }
    }

    @OnClick(R.id.refer_back_btn)
    public void onReferBackBtnClick() {
        onFabReferClick();
    }

    private boolean appInstalled(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
