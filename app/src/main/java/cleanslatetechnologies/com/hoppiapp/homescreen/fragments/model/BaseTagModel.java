package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.model;

/**
 * Created by manoj on 18/06/16.
 */
public abstract class BaseTagModel {
    private String text;

    public BaseTagModel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public abstract TagType getTagType();
}
