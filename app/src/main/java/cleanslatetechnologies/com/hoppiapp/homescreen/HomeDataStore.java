package cleanslatetechnologies.com.hoppiapp.homescreen;

import com.example.manoj.utilshelper.core.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.models.HomePageDataModel;
import cleanslatetechnologies.com.hoppiapp.models.TagModel;

/**
 * Created by manoj on 24/06/16.
 */
public class HomeDataStore {

    private static HomeDataStore instance;
    private HomePageDataModel homeData;

    private HomeDataStore() {
    }

    public static HomeDataStore getInstance() {
        if (instance == null) {
            instance = new HomeDataStore();
        }
        return instance;
    }


    public HomePageDataModel getHomeData() {
        return homeData;
    }

    public void setHomeData(HomePageDataModel homeData) {
        this.homeData = homeData;
    }

    public void updateLastTags(List<String> userTags) {
        if (homeData != null) {
            if (ArrayUtils.isCollectionFilled(userTags)) {
                String lastTag = userTags.get(userTags.size() - 1);
                List<TagModel> newTagList = homeData.getUserTags();
                if (homeData.getUserTags() == null) {
                    newTagList = new ArrayList<>();
                }
                int size = newTagList.size();
                if (size == 0) {
                    newTagList.add(new TagModel(lastTag));
                } else {
                    newTagList.set(size - 1, new TagModel(lastTag));
                }
                homeData.setUserTags(newTagList);
            }
        }
    }
}
