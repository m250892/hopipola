package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.model;

/**
 * Created by manoj on 18/06/16.
 */
public enum TagType {
    NONE,
    ADD_TAG,
    TAG;

    public static TagType fromValue(int value) {
        for (TagType rowType : TagType.values()) {
            if (rowType.ordinal() == value) {
                return rowType;
            }
        }
        return NONE;
    }
}
