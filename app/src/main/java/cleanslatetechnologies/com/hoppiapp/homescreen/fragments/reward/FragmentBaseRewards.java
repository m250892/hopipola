package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.reward;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseSingleActivityFragment;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.dialogs.ConfirmRewardDialog;

/**
 * Created by manoj on 18/06/16.
 */
public abstract class FragmentBaseRewards extends BaseSingleActivityFragment implements RecycelItemClicked {

    private List<RewardDataModel> myRewards = new ArrayList<>();
    private RewardsAdapter rewardsAdapter;
    private View buzzwaiter;
    private RecyclerView recyclerView;

    private TextView itemNameView;
    private ImageView itemIconView;


    @Override
    protected void initViews(View baseView) {
        recyclerView = (RecyclerView) baseView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        rewardsAdapter = new RewardsAdapter(getContext(), myRewards, this);
        recyclerView.setAdapter(rewardsAdapter);

        buzzwaiter = baseView.findViewById(R.id.buzz_waiter_view);
        itemNameView = (TextView) baseView.findViewById(R.id.item_name);
        itemIconView = (ImageView) baseView.findViewById(R.id.dish_type_icon);
    }

    protected void onMyRewardsChange(List<RewardDataModel> myRewards) {
        Log.d("manoj", "Rewards fetch success");
        this.myRewards.clear();
        this.myRewards.addAll(myRewards);
        rewardsAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemSelected(int position) {
        ConfirmRewardDialog confirmRewardDialog = ConfirmRewardDialog.newInstance(myRewards.get(position));
        confirmRewardDialog.show(getChildFragmentManager(), "Confirm");
    }

    public void onTreatSelected(RewardDataModel rewardData) {
        buzzwaiter.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        itemNameView.setText(rewardData.getNames());
        itemIconView.setImageResource(rewardData.getCategory().getDrawableId());
    }

    public class MyRewardsResponseModel {
        @SerializedName("message")
        private List<RewardDataModel> myRewards;

        public List<RewardDataModel> getMyRewards() {
            return myRewards;
        }
    }

    protected void fetchMyRewards() {
        Rest.connect().GET().load(getUrl()).as(MyRewardsResponseModel.class).withCallback(new RequestCallback<MyRewardsResponseModel>() {
            @Override
            public void onRequestSuccess(MyRewardsResponseModel data) {
                onMyRewardsChange(data.getMyRewards());
            }

            @Override
            public void onRequestError(RequestError error) {

            }
        });
    }

    protected abstract String getUrl();

}
