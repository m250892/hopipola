package cleanslatetechnologies.com.hoppiapp.homescreen.fragments.model;

/**
 * Created by manoj on 18/06/16.
 */
public class AddTagModel extends BaseTagModel {

    public AddTagModel(String text) {
        super(text);
    }

    @Override
    public TagType getTagType() {
        return TagType.ADD_TAG;
    }
}
