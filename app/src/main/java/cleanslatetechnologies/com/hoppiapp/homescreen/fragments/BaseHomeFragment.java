package cleanslatetechnologies.com.hoppiapp.homescreen.fragments;

import android.os.Bundle;

import com.example.manoj.utilshelper.ui.BaseSingleActivityFragment;

import cleanslatetechnologies.com.hoppiapp.homescreen.HomeDataStore;
import cleanslatetechnologies.com.hoppiapp.models.HomePageDataModel;

/**
 * Created by manoj on 12/06/16.
 */
public abstract class BaseHomeFragment extends BaseSingleActivityFragment {


    protected HomePageDataModel homeData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeData = HomeDataStore.getInstance().getHomeData();
    }

}


