package cleanslatetechnologies.com.hoppiapp.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 13/06/16.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("manoj", "Refreshed token: " + refreshedToken);
        UserProfileManager.getInstance().setFireBaseId(refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    //Send token to our server
    private void sendRegistrationToServer(String refreshedToken) {

    }
}
