package cleanslatetechnologies.com.hoppiapp.firebase;

import android.util.Log;

import com.example.manoj.utilshelper.managers.EventManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import cleanslatetechnologies.com.hoppiapp.dialogs.waitingfriendsresponse.FriendResponseEvent;
import cleanslatetechnologies.com.hoppiapp.notification.NotificationModel;
import cleanslatetechnologies.com.hoppiapp.notification.NotificationUtils;

public class FirebaseService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("manoj", "Notification recieve");
        Log.d("manoj", "From :  " + remoteMessage.getFrom());
        Log.d("manoj", "Type : " + remoteMessage.getData().get("type"));
        Log.d("manoj", "Data :  " + remoteMessage.getData());
        switch (remoteMessage.getData().get("type")) {
            case "2":
                String inviteeNumber = remoteMessage.getData().get("invitee_number");
                int response = Integer.parseInt(remoteMessage.getData().get("message"));
                FriendResponseEvent event = new FriendResponseEvent(inviteeNumber, response);
                EventManager.getDefaultEventBus().post(event);
                break;
            default:
                NotificationUtils.sendNotification(this, buildNotificationModel(remoteMessage.getData()));
        }
    }

    private NotificationModel buildNotificationModel(Map<String, String> data) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setNumber(data.get("sender_number"));
        notificationModel.setBody(data.get("body"));
        notificationModel.setNames(data.get("names"));
        notificationModel.setMessage(data.get("message"));
        notificationModel.setType(data.get("type"));
        return notificationModel;
    }


}
