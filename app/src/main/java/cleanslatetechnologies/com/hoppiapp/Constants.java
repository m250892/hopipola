package cleanslatetechnologies.com.hoppiapp;

/**
 * Created by manoj on 13/06/16.
 */
public class Constants {
    public static final int PARAM_LOGIN_REQUEST_CODE = 100;
    public static final int PARAM_COMBINE_RESULT_CODE = 101;
    public static final int PARAM_ADD_NEW_TAG_REQUEST_CODE = 102;

    public static final String PARAM_COMBINE_FRIENDS_DATA = "combine_friend_data";
    public static final String KEY_HOME_DATA = "home actvity data";
    public static final String SMS_ORIGIN = "CLNSLT";
    public static final String OTP_DELIMITER = ":";

    public static final String HOME_URL = "http://52.74.149.134/hoppi/welcome.php";
    public static final String LOGIN_URL = "http://52.74.149.134/hoppi/signin.php?user=";
    public static final String SIGN_UP_URL = "http://52.74.149.134/hoppi/request_sms.php";

    public static final String OTP_VERIFY_LOGIN = "http://52.74.149.134/hoppi/verify_otp_signin.php";
    public static final String OTP_VERIFY_SIGNIN = "http://52.74.149.134/hoppi/verify_otp.php";

    public static final String CONTACT_UPLOAD_URL = "http://52.74.149.134/hoppi/contact_upload.php";
    public static final String COMBINE_URL = "http://52.74.149.134/hoppi/sendNotification.php";
    public static final String MY_REWARDS = "http://52.74.149.134/hoppi/rewards.php";

    public static final String COMBINE_INVITE_RESPONSE = "http://52.74.149.134/hoppi/accept_invite.php";
    public static final String GET_FRIENDS_API = "http://52.74.149.134/hoppi/crossDB.php?user=";

    public static final String UPDAE_TAGS = "http://52.74.149.134/hoppi/addTags.php";

    public static final String CONTACT_FILE_NAME = "contacts";

    public static final String UPDATE_FIREBASE_ID = "http://52.74.149.134/hoppi/updateFID.php";
    public static final String SEARCH_TAG_URL = "http://52.74.149.134/hoppi/get_tags.php?chars=";
}
