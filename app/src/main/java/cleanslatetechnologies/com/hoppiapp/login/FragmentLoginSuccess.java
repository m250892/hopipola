package cleanslatetechnologies.com.hoppiapp.login;


import android.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.example.manoj.utilshelper.ui.BaseFragment;

import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLoginSuccess extends BaseFragment {

    public FragmentLoginSuccess() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_fragment_login_success;
    }

    @Override
    protected void initViews(View baseView) {
        TextView textView = (TextView) baseView.findViewById(R.id.name_view);
        textView.setText(UserProfileManager.getUserDetail().getName());

    }

    @OnClick(R.id.login_btn)
    public void onMyRewardClick() {
        getFragmentController().performOperation(ISignInController.MY_SIGN_UP_REWARD, null);
    }


}
