package cleanslatetechnologies.com.hoppiapp.login;

import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 14/06/16.
 */
public class FragmentSignUp extends BaseFragment {

    @Bind(R.id.inputName)
    public EditText nameView;

    @Bind(R.id.inputNumber)
    public EditText numberView;

    @Bind(R.id.error_view)
    public TextView errorView;

    @Bind(R.id.signup_btn)
    public TextView submitBtnView;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_sign_up;
    }

    @Override
    protected void initViews(View baseView) {
        numberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                float alpha = 0.4f;
                if (s.length() == 10) {
                    alpha = 1.0f;
                }
                submitBtnView.setAlpha(alpha);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        nameView.postDelayed(new Runnable() {
            @Override
            public void run() {
                showSoftKeyBoard(nameView);
            }
        }, 200);
    }

    @OnClick(R.id.signup_btn)
    public void onSignUpBtnClick() {
        errorView.setVisibility(View.GONE);
        String name = nameView.getText().toString();
        String phone = numberView.getText().toString();

        if (TextUtils.isEmpty(name)) {
            //showToastMessage("Please Enter name");
            showErrorMsg("Please Enter name");
            return;
        }

        if (!Utils.validate.isValidPhoneNumber(phone)) {
            //showToastMessage("Please Enter valid phone number");
            showErrorMsg("Please check your 10 digigt phone number");
            return;
        }
        doSignUp(name, phone);
    }

    private void doSignUp(String name, final String phone) {
        Uri.Builder builder = Uri.parse(Constants.SIGN_UP_URL).buildUpon();
        builder.appendQueryParameter("name", name);
        builder.appendQueryParameter("mobile", phone);
        builder.appendQueryParameter("email", "test@test.com");
        builder.appendQueryParameter("id", UserProfileManager.getInstance().getFireBaseId());

        String url = builder.build().toString();
        Rest.connect().GET().load(url).as(LoginResponse.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                showToastMessage("Otp Sent success");
                getFragmentController().performOperation(ISignInController.FRAGMENT_OTP_SCREEN, new OtpFragmentInput(phone, false));
            }

            @Override
            public void onRequestError(RequestError error) {
                showToastMessage(error.getMessage());
                showErrorMsg(error.getErrorMessage());
            }
        });

    }

    @OnClick(R.id.sign_btn)
    public void onSignInClick() {
        getFragmentController().performOperation(ISignInController.FRAGMENT_SIGN_IN, null);
    }

    @OnClick(R.id.back_btn)
    public void onBackBtnPress() {
        getFragmentController().onBackPressed();
    }


    private void showErrorMsg(String msg) {
        errorView.setVisibility(View.VISIBLE);
        errorView.setText(msg);
    }

}
