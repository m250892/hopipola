package cleanslatetechnologies.com.hoppiapp.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;

/**
 * Created by manoj on 14/06/16.
 */
public class FragmentLogin extends BaseFragment {

    @Bind(R.id.inputNumber)
    public EditText numberView;

    @Bind(R.id.login_btn)
    public TextView submitBtnView;

    @Bind(R.id.error_view)
    public TextView errorView;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void initViews(View baseView) {
        numberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                float alpha = 0.4f;
                if (s.length() == 10) {
                    alpha = 1.0f;
                }
                submitBtnView.setAlpha(alpha);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        numberView.postDelayed(new Runnable() {
            @Override
            public void run() {
                showSoftKeyBoard(numberView);
            }
        }, 100);
    }

    @OnClick(R.id.signup_btn)
    public void onSignUpBtnClick() {
        getFragmentController().performOperation(ISignInController.FRAGMENT_SIGN_UP, null);
    }

    @OnClick(R.id.login_btn)
    public void onLoginBtnClick() {
        errorView.setVisibility(View.GONE);
        String number = numberView.getText().toString();
        if (Utils.validate.isValidPhoneNumber(number)) {
            doLogin(number);
        } else {
            showErrorMsg("Please check you 10 digit phone number");
            //showToastMessage("Please Enter valid phone number");
        }
    }

    private void showErrorMsg(String msg) {
        errorView.setVisibility(View.VISIBLE);
        errorView.setText(msg);
    }


    @OnClick(R.id.back_btn)
    public void onBackBtnPress() {
        getFragmentController().onBackPressed();
    }

    private void doLogin(final String number) {
        String url = Constants.LOGIN_URL + number;
        Rest.connect().GET().load(url).as(LoginResponse.class).withCallback(new RequestCallback() {
            @Override
            public void onRequestSuccess(Object o) {
                showToastMessage("OTP send success");
                if (getFragmentController() != null) {
                    getFragmentController().performOperation(ISignInController.FRAGMENT_OTP_SCREEN, new OtpFragmentInput(number, true));
                }
            }

            @Override
            public void onRequestError(RequestError error) {
                //showToastMessage(error.getMessage());
                showErrorMsg(error.getErrorMessage());
            }
        });
    }
}

