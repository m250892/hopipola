package cleanslatetechnologies.com.hoppiapp.login;

import com.example.manoj.utilshelper.ui.IBaseFragmentController;

/**
 * Created by manoj on 14/06/16.
 */
public interface ISignInController extends IBaseFragmentController {

    int FRAGMENT_SIGN_IN = 1;
    int FRAGMENT_SIGN_UP = 2;
    int FRAGMENT_OTP_SCREEN = 3;
    int LOGIN_SUCCESS = 4;
    int MY_SIGN_UP_REWARD = 5;
}
