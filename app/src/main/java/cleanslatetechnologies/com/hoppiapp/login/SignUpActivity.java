package cleanslatetechnologies.com.hoppiapp.login;

import android.os.Bundle;

import com.example.manoj.utilshelper.interfaces.IPermissionsCallback;
import com.example.manoj.utilshelper.ui.BaseActivity;

import cleanslatetechnologies.com.hoppiapp.R;

public class SignUpActivity extends BaseActivity implements ISignInController, IPermissionsCallback {

    public static final String KEY_SOURCE = "key_source";
    boolean isSourceSignUp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        requestForPermission();
        if (getIntent() != null) {
            isSourceSignUp = getIntent().getBooleanExtra(KEY_SOURCE, false);
        }
        laodFirstFragment();
    }

    private void laodFirstFragment() {
        if (isSourceSignUp) {
            addFragmentInDefaultLayout(new FragmentSignUp(), false);
        } else {
            addFragmentInDefaultLayout(new FragmentLogin(), false);
        }

    }

    @Override
    public int getFragmentContainerId() {
        return R.id.fragment_container;
    }

    @Override
    public void performOperation(int operation, Object input) {
        switch (operation) {
            case FRAGMENT_SIGN_IN:
                replaceFragmentInDefaultLayout(new FragmentLogin());
                break;
            case FRAGMENT_SIGN_UP:
                replaceFragmentInDefaultLayout(new FragmentSignUp());
                break;
            case FRAGMENT_OTP_SCREEN:
                if (input == null || !(input instanceof OtpFragmentInput)) {
                    throw new IllegalArgumentException("Input should be instance of string, its phone number");
                }
                replaceFragmentInDefaultLayout(FragmentOtpScreen.newInstance((OtpFragmentInput) input));
                break;
            case LOGIN_SUCCESS:
                //set Activity result
                //send result to source actvity
                clearBackStack(true);
                addFragmentInDefaultLayout(new FragmentLoginSuccess());
                break;
            case MY_SIGN_UP_REWARD:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }

    public void requestForPermission() {
        requestPermission(IPermissionsCallback.READ_SMS, this);
    }

    @Override
    public void onPermissionRequestResult(int requestCode, boolean result) {
        if (requestCode == READ_SMS && result) {
            requestPermission(IPermissionsCallback.RECEIVE_SMS, this);
        }
    }
}
