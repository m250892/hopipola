package cleanslatetechnologies.com.hoppiapp.login;

import com.example.manoj.utilshelper.model.Model;

import java.io.Serializable;

/**
 * Created by manoj on 14/06/16.
 */
public class OtpFragmentInput extends Model implements Serializable {
    private String number;
    boolean isSourceLogin;

    public OtpFragmentInput(String number, boolean isSourceLogin) {
        this.number = number;
        this.isSourceLogin = isSourceLogin;
    }

    public String getNumber() {
        return number;
    }

    public boolean isSourceLogin() {
        return isSourceLogin;
    }
}
