package cleanslatetechnologies.com.hoppiapp.login;

import com.example.manoj.utilshelper.model.Model;

/**
 * Created by manoj on 14/06/16.
 */
public class LoginResponse extends Model {

    private ProfileData profile;

    public ProfileData getProfile() {
        return profile;
    }

    public String getName() {
        if (profile != null) {
            return profile.name;
        }
        return null;
    }

    public String getEmail() {
        if (profile != null) {
            return profile.email;
        }
        return null;
    }

    public String getMobile() {
        if (profile != null) {
            return profile.mobile;
        }
        return null;
    }

    private class ProfileData {
        private String name;
        private String email;
        private String mobile;


    }
}
