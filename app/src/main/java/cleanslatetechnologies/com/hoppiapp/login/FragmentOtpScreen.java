package cleanslatetechnologies.com.hoppiapp.login;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.managers.EventManager;
import com.example.manoj.utilshelper.rest.RequestError;
import com.example.manoj.utilshelper.rest.Rest;
import com.example.manoj.utilshelper.ui.BaseFragment;

import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.Constants;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.profile.UserDetail;
import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;
import cleanslatetechnologies.com.hoppiapp.smslistener.OtpEvent;

/**
 * Created by manoj on 14/06/16.
 */
public class FragmentOtpScreen extends BaseFragment {

    private static final String PARAMS_PHONE_NUMBER = "phone_number";
    @Bind(R.id.otp_input_view)
    public EditText otpView;

    @Bind(R.id.error_view)
    public TextView errorView;

    @Bind(R.id.submit_otp)
    public TextView submitBtnView;

    private String phoneNumber;
    private boolean isSourceLogin;

    public static BaseFragment newInstance(OtpFragmentInput data) {
        FragmentOtpScreen fragmentOtpScreen = new FragmentOtpScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PARAMS_PHONE_NUMBER, data);
        fragmentOtpScreen.setArguments(bundle);
        return fragmentOtpScreen;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OtpFragmentInput otpFragmentInput = (OtpFragmentInput) getArguments().getSerializable(PARAMS_PHONE_NUMBER);

        phoneNumber = otpFragmentInput.getNumber();
        isSourceLogin = otpFragmentInput.isSourceLogin();
    }

    @Override
    public void onDestroyView() {
        EventManager.getDefaultEventBus().unregister(this);
        super.onDestroyView();
    }

    @Subscribe
    public void onEvent(OtpEvent event) {
        if (event != null && event.getVerificationCode() != null) {
            otpView.setText(event.getVerificationCode());
            onSubmitOtp();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventManager.getDefaultEventBus().register(this);
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_otp;
    }

    @Override
    protected void initViews(View baseView) {
        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                float alpha = 0.4f;
                if (s.length() >= 3) {
                    alpha = 1.0f;
                }
                submitBtnView.setAlpha(alpha);
            }
        });
    }

    @OnClick(R.id.submit_otp)
    public void onSubmitOtp() {
        hideKeyboard();
        errorView.setVisibility(View.GONE);
        String otp = otpView.getText().toString();
        if (TextUtils.isEmpty(otp)) {
            //showToastMessage("please enter otp");
            showErrorMsg("please enter otp");
        } else {
            validateOtp(phoneNumber, otp);
        }
    }

    @OnClick(R.id.back_btn)
    public void onBackBtnPress() {
        getFragmentController().onBackPressed();
    }

    private void validateOtp(final String phoneNumber, String otp) {
        Uri.Builder builder = Uri.parse(getUrl()).buildUpon();
        builder.appendQueryParameter("user", phoneNumber);
        builder.appendQueryParameter("otp", otp);
        String url = builder.build().toString();

        Rest.connect().GET().load(url).as(LoginResponse.class).withCallback(new RequestCallback<LoginResponse>() {
            @Override
            public void onRequestSuccess(LoginResponse data) {
                //do validation
                UserDetail userDetail = new UserDetail();
                userDetail.setPhoneNumber(data.getMobile());
                userDetail.setEmailId(data.getEmail());
                userDetail.setName(data.getName());
                userDetail.setIsLoggedIn(true);
                UserProfileManager.getInstance().doLogin(userDetail);
                getFragmentController().performOperation(ISignInController.LOGIN_SUCCESS, phoneNumber);
            }

            @Override
            public void onRequestError(RequestError error) {
                //showToastMessage("Otp Verificaiton failed");
                showErrorMsg(error.getErrorMessage());
            }
        });
    }

    private void showErrorMsg(String msg) {
        errorView.setVisibility(View.VISIBLE);
        errorView.setText(msg);
    }

    public String getUrl() {
        if (isSourceLogin) {
            return Constants.OTP_VERIFY_LOGIN;
        }
        return Constants.OTP_VERIFY_SIGNIN;
    }
}
