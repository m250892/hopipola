package cleanslatetechnologies.com.hoppiapp.profile;

import com.example.manoj.utilshelper.model.Model;

/**
 * Created by manoj on 09/06/16.
 */
public class UserDetail extends Model {

    private boolean isLoggedIn;

    private String name;
    private String phoneNumber;
    private String emailId;


    public UserDetail() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void updateProfileData(UserDetail userDetail) {
        setName(userDetail.getName());
        setPhoneNumber(userDetail.getPhoneNumber());
        setEmailId(userDetail.getPhoneNumber());
    }

    public void removeData() {
        setName(null);
        setEmailId(null);
        setPhoneNumber(null);
    }
}
