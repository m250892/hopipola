package cleanslatetechnologies.com.hoppiapp.profile;

import com.example.manoj.utilshelper.managers.SharedPrefsManager;

import java.util.ArrayList;
import java.util.List;

import cleanslatetechnologies.com.hoppiapp.models.ContactDataModel;

/**
 * Created by manoj on 09/06/16.
 */
public class UserProfileManager {

    private static final String KEY_USER_DETAIL = "User Detail Key";
    private static final String KEY_CONTACT_LIST = "User Contacts List";
    private static final String KEY_FIREBASE_ID = "firebaseid";
    private static final java.lang.String KEY_CONTACT_UPLOADED = "contact_uploaded";
    private String fireBaseId;
    private static UserProfileManager instance;
    private UserDetail userDetail;
    private boolean isContactUploaded;

    private List<ContactDataModel> userContacts;
    private List<String> firendsContacts;


    private UserProfileManager() {
        updateUserDetail();
    }

    public synchronized static UserProfileManager getInstance() {
        if (instance == null) {
            instance = new UserProfileManager();
        }
        return instance;
    }

    public synchronized static void init() {
        if (instance == null) {
            instance = new UserProfileManager();
        }
    }

    public void saveUserDetail() {
        SharedPrefsManager.getInstance().setObject(KEY_USER_DETAIL, userDetail);
    }

    public synchronized void updateUserDetail() {
        fireBaseId = SharedPrefsManager.getInstance().getString(KEY_FIREBASE_ID);
        userDetail = SharedPrefsManager.getInstance().getObject(KEY_USER_DETAIL, UserDetail.class);
        isContactUploaded = SharedPrefsManager.getInstance().getBoolean(KEY_CONTACT_UPLOADED);
        if (userDetail == null) {
            userDetail = new UserDetail();
            //doLogin(userDetail);
        }
        updateContactsSharedPref();
    }

    public void updateContactsSharedPref() {
        //userContacts = SharedPrefsManager.getInstance().getObjectList(KEY_CONTACT_LIST, ContactDataModel.class);
        if (userContacts == null) {
            userContacts = new ArrayList<>();
        }
    }

    public static boolean isUserLogin() {
        return getUserDetail().isLoggedIn();
    }


    public void updateContacts(List<ContactDataModel> contactDataModels) {
        userContacts.clear();
        userContacts.addAll(contactDataModels);
        //SharedPrefsManager.getInstance().setObjectList(KEY_CONTACT_LIST, userContacts);
    }

    public void clearContacts() {
        SharedPrefsManager.getInstance().removeKey(KEY_CONTACT_LIST);
        userContacts.clear();
    }

    public List<ContactDataModel> getUserContacts() {
        return userContacts;
    }

    public static UserDetail getUserDetail() {
        return getInstance().userDetail;
    }

    public synchronized void doLogin(UserDetail userDetail) {
        getUserDetail().setIsLoggedIn(true);
        getUserDetail().updateProfileData(userDetail);
        saveUserDetail();
    }

    public synchronized void doLogout() {
        getUserDetail().setIsLoggedIn(false);
        SharedPrefsManager.getInstance().removeKey(KEY_USER_DETAIL);
        getUserDetail().removeData();
        clearContacts();
    }

    public String getFireBaseId() {
        return fireBaseId;
    }

    public void setFireBaseId(String fireBaseId) {
        this.fireBaseId = fireBaseId;
        SharedPrefsManager.getInstance().setString(KEY_FIREBASE_ID, fireBaseId);
    }

    public boolean isContactUploaded() {
        return isContactUploaded;
    }

    public void setContactUploaded(boolean contactUploaded) {
        isContactUploaded = contactUploaded;
        SharedPrefsManager.getInstance().setBoolean(KEY_CONTACT_UPLOADED, contactUploaded);
    }

    public List<String> getFirendsContacts() {
        return firendsContacts;
    }

    public void setFirendsContacts(List<String> firendsContacts) {
        this.firendsContacts = firendsContacts;
    }

}
