package cleanslatetechnologies.com.hoppiapp.utils;

import android.os.CountDownTimer;

public class CustomTimer {

    private static CustomTimer instance;

    private ICountDownTimeCallback callback;

    private CountDownTimer countDownTimer;
    private long currentRemainingTime;

    private CustomTimer() {
    }

    public long getCurrentValue() {
        return currentRemainingTime;
    }

    public static CustomTimer getInstance() {
        if (instance == null) {
            instance = new CustomTimer();
        }
        return instance;
    }

    public void startTimer(int timeInSeconds, int intervalInSec) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(timeInSeconds * 1000, intervalInSec * 1000) {
            public void onTick(long millisUntilFinished) {
                currentRemainingTime = millisUntilFinished / 1000;
                if (callback != null) {
                    callback.onTimerUpdate(currentRemainingTime);
                }
            }

            public void onFinish() {
                if (callback != null) {
                    callback.onTimerFinish();
                }
                countDownTimer = null;
            }
        };
        countDownTimer.start();
    }

    public void registerTimerCallback(ICountDownTimeCallback callback) {
        this.callback = callback;
    }

    public void unregisterTimberCallback() {
        this.callback = null;
    }

    public interface ICountDownTimeCallback {

        void onTimerUpdate(long seconds);

        void onTimerFinish();
    }

}
