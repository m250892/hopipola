package cleanslatetechnologies.com.hoppiapp;

import com.example.manoj.utilshelper.ui.BaseApplication;

import cleanslatetechnologies.com.hoppiapp.profile.UserProfileManager;

/**
 * Created by manoj on 09/06/16.
 */
public class MainApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        UserProfileManager.init();
    }
}
