package cleanslatetechnologies.com.hoppiapp.feedback;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cleanslatetechnologies.com.hoppiapp.R;
import cleanslatetechnologies.com.hoppiapp.tags.TagActivity;

public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.positive_feedback)
    public void onPositiveFeedback() {
        Intent intent = new Intent(this, TagActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.negative_feedback)
    public void onNegativeFeedback() {
        Intent intent = new Intent(this, TagActivity.class);
        startActivity(intent);
    }
}
