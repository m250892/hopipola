package cleanslatetechnologies.com.hoppiapp.smslistener;

/**
 * Created by manoj on 14/06/16.
 */
public class OtpEvent {
    private String verificationCode;

    public OtpEvent(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getVerificationCode() {
        return verificationCode;
    }
}
